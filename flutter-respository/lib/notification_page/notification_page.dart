import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NotificationPage extends StatefulWidget {
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Image.asset("assets/appbar.png", height: 70, fit: BoxFit.cover,),
        Container(
          margin: EdgeInsets.only(
            top: 20,
          ),
          child: Stack(
            children: <Widget>[
              IconButton(
                onPressed: () => Navigator.pop(context),
                icon: Icon(Icons.chevron_left, color: Colors.white, size: 34,),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 12
                ),
                width: MediaQuery.of(context).size.width,
                child: Text("NOTIFIKASI", style: TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 30,
                  bottom: 50,
                ),
                child: ListView.builder(
                  itemCount: 10,
                  itemBuilder: (context, index) => listNotif(),
                ),
              ),
            ],
          )
        ),

      ],
    );
  }

  Widget listNotif() {
    return InkWell(
      child: Container(
        decoration: BoxDecoration(
          color: Colors.grey[200],
        ),
        margin: EdgeInsets.only(
          bottom: 1,
        ),
        padding: EdgeInsets.only(
          left: 20,
          right: 20,
          top: 14,
          bottom: 14,
        ),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                SvgPicture.asset(
                  "assets/notification.svg", 
                  height: 30, 
                  color: Colors.grey[600],
                ),
                Container(width: 20,),
                Expanded(
                  child: Text(
                    "Durasi kelas belanjar anda tinggal 7 hari lagi.",
                    style: TextStyle(
                      color: Colors.grey[600]
                    ),
                  ),
                ),
                Container(width: 5,),
                SvgPicture.asset(
                  "assets/eye.svg", 
                  height: 7, 
                  color: Colors.blue,
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(child: Container(),),
                Text(
                  "17 Maret 2019",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontSize: 10,
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}