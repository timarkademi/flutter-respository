import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
class NilaiKuisPage extends StatefulWidget {
  @override
  _NilaiKuisPageState createState() => _NilaiKuisPageState();
}

class _NilaiKuisPageState extends State<NilaiKuisPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            headerWidget(),
            listTileBlue(
              "assets/medal.svg",
              "NILAI KUIS"
            ),
            Container(
              padding: EdgeInsets.only(
                top: 20,
                left: 15,
                right: 15,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  
                  rowNilai("Tes Wawasan Kebangsaan (TKW) 3","85 / 175"),
                  rowNilai("Tes Intelegensi Umum (TIU) 3","115 / 150"),
                  rowNilai("Kuis Bootstrap","10 / 10"),
                  
                  Container(height: 30,),
                  
                ],
              ),
            )
          ]
        ),
      ),
    );
  }

  void _save() {

  }

  Widget headerWidget() {
    return Stack(
      children: <Widget>[
        Image.asset('assets/profile-header.png'),
        Container(
          margin: EdgeInsets.only(
            top: 20,
          ),
          child: Stack(
            children: <Widget>[
              
              Container(
                margin: EdgeInsets.only(
                  top: 12
                ),
                width: MediaQuery.of(context).size.width,
                child: Text("DASHBOARD", style: TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 65
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Image.asset("assets/avatar.png", width: 150,),
                        Text(
                          "Hilman Fajrian",
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                        Text(
                          "HilmanFajrian@gmail.com",
                          style: TextStyle(
                            fontSize: 11,
                          ),
                        ),
                        Container(height: 30,),
                        
                      ],
                    ),
                  ],
                ),
              ),
              IconButton(
                onPressed: () => Navigator.pop(context),
                icon: Icon(Icons.chevron_left, color: Colors.white, size: 34,),
              ),
            ],
          )
        ),
      ],
    );
  }

  Widget listTileBlue(String svg, String title) {
    return InkWell(
      child: Container(
        color: Color.fromRGBO(239, 247, 254, 1),
        margin: EdgeInsets.only(
          bottom: 3,
        ),
        padding: EdgeInsets.only(
          top: 3,
          bottom: 3,
          left: 15,
          right: 5,
        ),
        child: ListTile(
          leading: SvgPicture.asset(
            svg,
            height: 25,
            color: Colors.black,
          ),
          title: Text(
            title,
            style: TextStyle(
              fontSize: 20
            ),
          ),
        ),
      ),
    );
  }

  Widget rowNilai(String title, String nilai) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
            top: 20,
            bottom: 20,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                title,
                style: TextStyle(
                  fontSize: 16,

                ),
              ),
              Text(
                nilai,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
        Divider(height: 3,),
      ],
    );
  }
}