class Ulasan {
  final int id;
  final String photo;
  final String title;
  final String description;
  final int ratingStar;

  Ulasan({
    this.id, 
    this.photo,
    this.title,
    this.description,
    this.ratingStar,
  });
}

List<Ulasan> ulasanList = [
  Ulasan(
    id: 1,
    photo: "",
    title: "Mantap",
    description: "Non esse ex anim do laborum aliqua veniam Lorem do veniam consequat. Sint irure pariatur aute non officia ut cillum pariatur deserunt. Culpa ea dolore est anim ad deserunt reprehenderit ex elit quis magna.",
    ratingStar: 5,
  ),
  Ulasan(
    id: 2,
    photo: "",
    title: "PAS UNTUK PEMULA MAUPUN PROFESIONAL",
    description: "Non esse ex anim do laborum aliqua veniam Lorem do veniam consequat. Sint irure pariatur aute non officia ut cillum pariatur deserunt. Culpa ea dolore est anim ad deserunt reprehenderit ex elit quis magna.",
    ratingStar: 5,
  ),
  Ulasan(
    id: 3,
    photo: "",
    title: "PAS UNTUK PEMULA MAUPUN PROFESIONAL",
    description: "Non esse ex anim do laborum aliqua veniam Lorem do veniam consequat. Sint irure pariatur aute non officia ut cillum pariatur deserunt. Culpa ea dolore est anim ad deserunt reprehenderit ex elit quis magna.",
    ratingStar: 5,
  ),
  Ulasan(
    id: 4,
    photo: "",
    title: "PAS UNTUK PEMULA MAUPUN PROFESIONAL",
    description: "Non esse ex anim do laborum aliqua veniam Lorem do veniam consequat. Sint irure pariatur aute non officia ut cillum pariatur deserunt. Culpa ea dolore est anim ad deserunt reprehenderit ex elit quis magna.",
    ratingStar: 5,
  ),
];