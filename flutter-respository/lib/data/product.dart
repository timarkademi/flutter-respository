List<Product> productList = [
  Product(
    id: 1,
    title: 'AWCAKADEMI:Meningkatkan Penjuala..',
    rating: 4.5,
    student: 100000,
    imageUrl: "https://cdn.arkademi.com/asset/img/20190518010256/ARKADEMI-feature-kelas-andhika-460x241.jpg",
    price: 0,
  ),
  Product(
    id: 2,
    title: 'Inbound Marketing',
    rating: 4.5,
    student: 1000,
    imageUrl: "https://cdn.arkademi.com/asset/img/20190518010352/Feature-inbound-marketing-baru-460x240.jpg",
    price: 0,
  ),
  
];

List<Product> productList2 = [
  Product(
    id: 1,
    title: 'Mulai usaha dari kecil dengan modal sendiri',
    rating: 4.5,
    student: 100000,
    imageUrl: "https://cdn.arkademi.com/asset/img/20190518010256/ARKADEMI-feature-kelas-andhika-460x241.jpg",
    price: 2500000,
    priceAfterDisc: 995000,
  ),
  Product(
    id: 2,
    title: 'Panduan teknis memulai dan mengembangkan usaha',
    rating: 4.5,
    student: 1000,
    imageUrl: "https://cdn.arkademi.com/asset/img/20190518010352/Feature-inbound-marketing-baru-460x240.jpg",
    price: 500000,
    priceAfterDisc: 95000,
  ),
  
];


class Product {
  final int id;
  final String title;
  final double rating;
  final int student;
  final String imageUrl;
  final int price;
  final int priceAfterDisc;

  Product({
    this.id, 
    this.title,
    this.rating,
    this.student,
    this.imageUrl, 
    this.price, 
    this.priceAfterDisc, 
  });
}