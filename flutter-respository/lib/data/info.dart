List<Info> infoList = [
  Info(
    id: 1,
    icon: "",
    label: "Diagnosa Usaha Rintisan"
  ),
  Info(
    id: 2,
    icon: "",
    label: "Tes Teknik Kewirausahaan"
  ),
  
];


class Info {
  final int id;
  final String icon;
  final String label;

  Info({
    this.id, 
    this.icon, 
    this.label,
  });
}