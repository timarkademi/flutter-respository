List<Banner> bannerList = [
  Banner(
    id: 1,
    image: 'https://staging-arkademi.kinsta.cloud/wp-content/uploads/2017/10/Feature-inbound-marketing-baru-460x240.jpg',
  ),
  Banner(
    id: 2,
    image: 'https://staging-arkademi.kinsta.cloud/wp-content/uploads/2017/10/feature-bootstrap-baru-460x240.jpg',
  ),
  
];


class Banner {
  final int id;
  final String image;

  Banner({
    this.id, 
    this.image, 
  });
}