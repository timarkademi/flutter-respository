import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'dashedrect.dart';
class AktivitasPage extends StatefulWidget {
  @override
  _AktivitasPageState createState() => _AktivitasPageState();
}

class _AktivitasPageState extends State<AktivitasPage> {
  List<String> aktivitasList = [
    "Siswa Hilman Fajrian telah menyelesaikan seri 5 kelas Bootstrap 15",
    "Siswa Hilman Fajrian telah menyelesaikan seri 5 kelas Bootstrap 15",
    "Siswa Hilman Fajrian telah menyelesaikan seri 5 kelas Bootstrap 15",
    "Siswa Hilman Fajrian telah menyelesaikan seri 5 kelas Bootstrap 15",
    "Siswa Hilman Fajrian telah menyelesaikan seri 5 kelas Bootstrap 15",
    "Siswa Hilman Fajrian telah menyelesaikan seri 5 kelas Bootstrap 15",
    "Siswa Hilman Fajrian telah menyelesaikan seri 5 kelas Bootstrap 15",
    "Siswa Hilman Fajrian telah menyelesaikan seri 5 kelas Bootstrap 15",
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            headerWidget(),
            listTileBlue(
              "assets/aktivitas.svg",
              "AKTIVITAS"
            ),
            Container(
              padding: EdgeInsets.only(
                top: 0,
                left: 15,
                right: 15,
              ),
              child: Column(
                children: [
                  for(int i = 0; i < aktivitasList.length; i++) 
                    timeLine(aktivitasList[i]),
                ]
                
              ),
            )
          ]
        ),
      ),
    );
  }


  Widget headerWidget() {
    return Stack(
      children: <Widget>[
        Image.asset('assets/profile-header.png'),
        Container(
          margin: EdgeInsets.only(
            top: 20,
          ),
          child: Stack(
            children: <Widget>[
              
              Container(
                margin: EdgeInsets.only(
                  top: 12
                ),
                width: MediaQuery.of(context).size.width,
                child: Text("DASHBOARD", style: TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 65
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Image.asset("assets/avatar.png", width: 150,),
                        Text(
                          "Hilman Fajrian",
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                        Text(
                          "HilmanFajrian@gmail.com",
                          style: TextStyle(
                            fontSize: 11,
                          ),
                        ),
                        Container(height: 30,),
                        
                      ],
                    ),
                  ],
                ),
              ),
              IconButton(
                onPressed: () => Navigator.pop(context),
                icon: Icon(Icons.chevron_left, color: Colors.white, size: 34,),
              ),
            ],
          )
        ),
      ],
    );
  }

  Widget listTileBlue(String svg, String title) {
    return InkWell(
      child: Container(
        color: Color.fromRGBO(239, 247, 254, 1),
        margin: EdgeInsets.only(
          bottom: 3,
        ),
        padding: EdgeInsets.only(
          top: 3,
          bottom: 3,
          left: 15,
          right: 5,
        ),
        child: ListTile(
          leading: SvgPicture.asset(
            svg,
            height: 25,
            color: Colors.black,
          ),
          title: Text(
            title,
            style: TextStyle(
              fontSize: 20
            ),
          ),
        ),
      ),
    );
  }

  Widget timeLine(String title) {
    return Stack(
      children: <Widget>[
        new Padding(
          padding: const EdgeInsets.only(
            top: 20,
            bottom: 5,
            left: 40.0
          ),
          child: Container(
            child: Text(title),
          ),
        ),
        new Positioned(
          top: 0.0,
          bottom: 0.0,
          left: 15.0,
          child: new Container(
            height: double.infinity,
            width: 1.0,
            color: Colors.transparent,
            child: DashedRect(color: Colors.black, strokeWidth: 1.0, gap: 1.0,),
          ),
        ),
        new Positioned(
          top: 20.0,
          left: 5.0,
          child: new Container(
            margin: new EdgeInsets.all(5.0),
            height: 10.0,
            width: 10.0,
            decoration: new BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.black),
          ),
        )
      ],
    );
  }

}