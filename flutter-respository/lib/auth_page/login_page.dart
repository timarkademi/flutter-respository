import 'package:arkademi/main_page/main_page.dart';
import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';
import 'dart:async';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class LoginPage extends StatefulWidget {
  
  @override
  _LoginPageState createState() => _LoginPageState();
}


class _LoginPageState extends State<LoginPage> {

   String token = "";
  final user = new TextEditingController();
  final pass = new TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Card(
          margin: EdgeInsets.only(
            top: 20,
          ),
          color: Colors.white,
          elevation: 5,
          child: Container(
            
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              
            ),
            width: 280,
            child: Column(
              children: <Widget>[
                TextField(
                  controller : user,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Mdi.email),
                    hintText: "Email atau username",

                  ),
                  keyboardType: TextInputType.emailAddress,
                ),
                Container(height: 10,),
                TextField(
                  controller : pass,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Mdi.lock),
                    hintText: "Password",
                    suffixIcon: IconButton(
                      onPressed: () {},
                      icon: Icon(Mdi.eye),
                    )
                  ),
                  obscureText: true,
                  keyboardType: TextInputType.text,
                ),
                Container(height: 30,),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7)
                    ),
                    onPressed: _auth,
                    color: Color.fromRGBO(30, 130, 15, 1),
                    child: Text("MASUK", style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),),
                  ),
                ),
                
                
              ],
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            top: 20,
            bottom: 10,
          ),
          child: Text("Lupa password?", style: TextStyle(
            fontSize: 16,
            decoration: TextDecoration.underline,
            color: Colors.black87,
          ),),
        ),
        Container(
          margin: EdgeInsets.only(
            top: 10,
            bottom: 25 ,
          ),
          child: Text("Masuk atau daftar dengan akun FB/Google", style: TextStyle(
            color: Colors.black,
            fontSize: 16,
            fontWeight: FontWeight.w500,
          ),),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FlatButton(
              onPressed: (){},
              child: Container(
                decoration: BoxDecoration(
                  color: Color.fromRGBO(25, 39, 130, 1),
                  borderRadius: BorderRadius.circular(100)
                ),
                padding: EdgeInsets.fromLTRB(10, 8, 10, 12),
                child: Icon(Mdi.facebook, color: Colors.white, size: 40,),
              ),
            ),
            FlatButton(
              onPressed: (){},
              child: Container(
                decoration: BoxDecoration(
                  color: Color.fromRGBO(177, 6, 6, 1),
                  borderRadius: BorderRadius.circular(100)
                ),
                padding: EdgeInsets.fromLTRB(10, 8, 10, 12),
                child: Icon(Mdi.google, color: Colors.white, size: 40,),
              ),
            ),
            
          ],
        ),
        Container(height: 60,),
      ],
    );
  }

  Future<http.Response> _auth() async {
    //SharedPreferences prefs = await SharedPreferences.getInstance();

    //prefs.setBool('user_login', true);

    //Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
    //MainPage()), (Route<dynamic> route) => false);

      final data = {
       "username": user.text,
        "password": pass.text, 	
        "client_id":"WF9PnyccBmbrEW3TKt4drFB",
        "state":")6cYyNr0",             
    };
  //encode Map to JSON
 
  var body = json.encode(data);
    //print(body);   
    final response = await http.post(
        "https://staging.arkademi.com/wp-json/wplms/v1/user/signin",
        headers: {"Content-Type": "application/json"},
        body: body);

      print(response.body);
      var datauser = json.decode(response.body);

    
  if(datauser['status'] == true){
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('user_login', true);
    prefs.setInt('user_id', datauser['user']['id']);
    prefs.setString('user_name', datauser['user']['name']);
    prefs.setString('user_email', datauser['user']['email']);
    prefs.setString('user_avatar', datauser['user']['avatar']);

    prefs.setString('token_access', datauser['token']['access_token']);
    prefs.setString('user_client_id', datauser['token']['client_id']);
   //prefs.setString('user_uder_id', datauser['token']['user_id']);
    //prefs.setString('user_expires', datauser['utokenser']['expires']);

    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
    MainPage()), (Route<dynamic> route) => false);
  }
  
  }
}