import 'package:arkademi/main_page/main_page.dart';
import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';
import 'package:http/http.dart' as http;
import 'package:arkademi/auth_page/auth_page.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {


  String session ;
  TextEditingController  controllerUsername =  new TextEditingController();
  TextEditingController  controllerPassword =  new TextEditingController();
  TextEditingController  controllerEmail =  new TextEditingController(); 



Future<List> addData() async {
 
   final data =  {
    "username":"${controllerUsername.text}",
    "password":"${controllerPassword.text}",
    "email":"${controllerEmail.text}",
    "client_id":"WF9PnyccBmbrEW3TKt4drFB",
    "state":")6cYyNr0",
    "device":null,
    "platform":null,
    "model":null
    };
  
  var row = json.encode(data);
  final response = await http.post("https://staging.arkademi.com/wp-json/wplms/v1/user/register", body: row);
  var datauser = json.decode(response.body);


  if(datauser['status'] == true){
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('user_login', true);
    prefs.setInt('user_id', datauser['user']['id']);
    prefs.setString('user_name', datauser['user']['name']);
    prefs.setString('user_email', datauser['user']['email']);
    prefs.setString('user_avatar', datauser['user']['avatar']);
    prefs.setString('token_access', datauser['token']['access_token']);
    prefs.setString('user_client_id', datauser['token']['client_id']);

    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
    MainPage()), (Route<dynamic> route) => false);

  }
}


//  void addData(){
    
    //final data =  {
    //"username":"${controllerUsername.text}",
    //"password":"${controllerPassword.text}",
    //"email":"${controllerEmail.text}",
    //"client_id":"OSBE73Zm2uNQMai0E4NNTaD",
    //"state":")6cYyNr0",
    //"device":null,
    //"platform":null,
    //"model":null
    //};
    //final body = json.encode(data);
    //var url =  'https://staging-arkademi.kinsta.cloud/wp-json/wplms/v1/user/register';
    //http.post(url, headers: {"Content-Type": "application/json"},body : body);

//  }




  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            top: 20,
            bottom: 20 ,
          ),
          child: Text("Daftar dengan akun FB/Google", style: TextStyle(
            color: Colors.black,
            fontSize: 16,
            fontWeight: FontWeight.w400,
          ),),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FlatButton(
              onPressed: (){},
              child: Container(
                decoration: BoxDecoration(
                  color: Color.fromRGBO(25, 39, 130, 1),
                  borderRadius: BorderRadius.circular(100)
                ),
                padding: EdgeInsets.fromLTRB(10, 8, 10, 12),
                child: Icon(Mdi.facebook, color: Colors.white, size: 40,),
              ),
            ),
            FlatButton(
              onPressed: (){},
              child: Container(
                decoration: BoxDecoration(
                  color: Color.fromRGBO(177, 6, 6, 1),
                  borderRadius: BorderRadius.circular(100)
                ),
                padding: EdgeInsets.fromLTRB(10, 8, 10, 12),
                child: Icon(Mdi.google, color: Colors.white, size: 40,),
              ),
            ),
            
          ],
        ),
        Container(
          margin: EdgeInsets.only(
            top: 20,
            bottom: 10 ,
          ),
          child: Text("Atau mendaftar dengan alamat email", style: TextStyle(
            color: Colors.black,
            fontSize: 14,
            fontWeight: FontWeight.w400,
          ),),
        ),
        Card(
          margin: EdgeInsets.only(
            top: 10,
            bottom: 60
          ),
          color: Colors.white,
          elevation: 5,
          child: Container(
            
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              
            ),
            width: 280,
            child: Column(
              children: <Widget>[
                TextField(
                  controller: controllerUsername,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Mdi.accountCircleOutline),
                    hintText: "Username",

                  ),
                  keyboardType: TextInputType.text,
                ),
               /* Container(height: 10,),
                TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Mdi.accountCardDetailsOutline),
                    hintText: "Nama",
                  ),
                  keyboardType: TextInputType.text,
                ), */
                Container(height: 10,),
                TextField(
                  controller: controllerEmail,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Mdi.email),
                    hintText: "Email",

                  ),
                  keyboardType: TextInputType.emailAddress,
                ),
                Container(height: 10,),
                TextField(
                  controller: controllerPassword,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Mdi.lock),
                    hintText: "Password",
                    suffixIcon: IconButton(
                      onPressed: () {},
                      icon: Icon(Mdi.eye),
                    )
                  ),
                  obscureText: true,
                  keyboardType: TextInputType.text,
                ),
                Container(height: 30,),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7)
                    ),
                    onPressed: (){
                      addData();
                    },
                    color: Color.fromRGBO(255, 90, 0, 1),
                    child: Text("DAFTAR", style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),), 
                  ),
                ),
                
                
              ],
            ),
          ),
        ),
        
        
      ],
    );
  }
}