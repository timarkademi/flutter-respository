import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class KelasSayaPage extends StatefulWidget {
  @override
  _KelasSayaPageState createState() => _KelasSayaPageState();
}

class _KelasSayaPageState extends State<KelasSayaPage> {

   
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            headerWidget(),
            KelasSayaList(),
            Container(height: 40,),
          ],
        ),
      ),
    );
  }


  Widget headerWidget() {
    return Stack(
      children: <Widget>[
        Image.asset('assets/profile-header.png', width: MediaQuery.of(context).size.width,),
        Container(
          margin: EdgeInsets.only(
            top: 20,
          ),
          child: Stack(
            children: <Widget>[
              
              Container(
                margin: EdgeInsets.only(
                  top: 12
                ),
                width: MediaQuery.of(context).size.width,
                child: Text("KELAS SAYA", style: TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 65
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Image.asset(
                          "assets/kelas-saya.png", 
                          width: MediaQuery.of(context).size.width - 130,
                        ),
                        
                        Container(height: 20,),
                        
                      ],
                    ),
                  ],
                ),
              ),
              IconButton(
                onPressed: () => Navigator.pop(context),
                icon: Icon(Icons.chevron_left, color: Colors.white, size: 34,),
              ),
            ],
          )
        ),
      ],
    );
  }
}

class KelasSayaList extends StatefulWidget {


  @override
  _KelasSayaListState createState() => _KelasSayaListState(





  );
}

class _KelasSayaListState extends State<KelasSayaList> {

  List dataJSON = [];
  Future<List>  ambildata() async {
  http.Response hasil = await http.get(
    Uri.encodeFull("https://staging.arkademi.com/wp-json/wplms/v1/course"), headers: {
      "Accept":"application/json"
    }
  );
  this.setState((){
    dataJSON = json.decode(hasil.body);
});
}
  @override
  void initState() {
    super.initState();
    this.ambildata();
  }

  int _selectedIndex = 0;
  @override
  Widget build(BuildContext context) {

    //print(dataJSON);
    return Stack(
      children: <Widget>[
        Center(
          child: Container(
            margin: EdgeInsets.only(
              top: 10,
            ),
            padding: EdgeInsets.only(
              left: 15,
              right: 15,
            ),
            child: Card(
              elevation: 5,
              child: Container(
                padding: EdgeInsets.only(
                  top: 40,
                  left: 10,
                  right: 10,
                ),
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: <Widget>[
                      for(int i = 0; i < dataJSON.length; i++) kelasItem(i)        
                  ],
                )
              ),
            ),
          ),
        ),
        tabWidget(),
      ],
    );
  }

  Widget tabWidget() {
    return Center(
      child: Container(
        width: 160,
        height: 35,
        decoration: BoxDecoration(
          color: Color.fromRGBO(9, 120, 191, 1),
          borderRadius: BorderRadius.circular(50),

        ),
        padding: EdgeInsets.fromLTRB(5,5,5,5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            InkWell(
              onTap: () {
                setState(() {
                  _selectedIndex = 0;
                });
              },
              child: Container(
                width: 50,
                height: 28,
                decoration: BoxDecoration(
                  color: _selectedIndex == 0 ? Colors.white : Colors.transparent,
                  borderRadius: BorderRadius.circular(50),
                ),
                
                child: Center(child: Text("Semua", style: TextStyle(
                  fontSize: 11,
                  fontWeight: FontWeight.bold,
                  color: _selectedIndex == 0 ? Colors.black : Colors.white,
                ),)),
              ),
            ),
            InkWell(
              onTap: () {
                setState(() {
                  _selectedIndex = 1;
                });
              },
              child: Container(
                width: 50,
                height: 28,
                decoration: BoxDecoration(
                  color: _selectedIndex == 1 ? Colors.white : Colors.transparent,
                  borderRadius: BorderRadius.circular(50),
                ),
                
                child: Center(child: Text("Aktif", style: TextStyle(
                  fontSize: 11,
                  fontWeight: FontWeight.bold,
                  color: _selectedIndex == 1 ? Colors.black : Colors.white,
                ),)),
              ),
            ),
            InkWell(
              onTap: () {
                setState(() {
                  _selectedIndex = 2;
                });
              },
              child: Container(
                width: 50,
                height: 28,
                decoration: BoxDecoration(
                  color: _selectedIndex == 2 ? Colors.white : Colors.transparent,
                  borderRadius: BorderRadius.circular(50),
                ),
                
                child: Center(child: Text("Selesai", style: TextStyle(
                  fontSize: 11,
                  fontWeight: FontWeight.bold,
                  color: _selectedIndex == 2 ? Colors.black : Colors.white,
                ),)),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget kelasItem(i) {
    return Container(
      margin: EdgeInsets.only(
        bottom: 8,
      ),
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: 90,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8)
          ),
          color: Colors.grey[200],
          child: InkWell(
            child: Row(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(50),
                    bottomLeft: Radius.circular(8),
                    topLeft: Radius.circular(8),
                  ),
                  child: Image.network(dataJSON[i]['data']['featured_image'],
                    height: 90,
                    width: 110,
                    fit: BoxFit.cover,
                  ),
                ),
                
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(
                      left: 15,
                      right: 10
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          dataJSON[i]['data']['name'],
                          style: TextStyle(
                            fontSize: 14,
                          ),
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Container(height: 5,),
                        Text(
                          "Mentor: ${dataJSON[i]['data']['instructor']['name']}",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Container(height: 5,),
                        LinearPercentIndicator(
                          width: MediaQuery.of(context).size.width - 201,
                          lineHeight: 8.0,
                          percent: 0.9,
                          progressColor: Color.fromRGBO(254, 86, 23, 1),
                        ),
                      ],
                    )
                  ),
                ),
              ],
            )
          ),
        ),
      ),
    );
  }
}