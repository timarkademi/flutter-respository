import 'package:arkademi/auth_page/auth_page.dart';
import 'package:arkademi/auth_page/login_page.dart';
import 'package:arkademi/data/ulasan.dart' as ulasan;
import 'package:arkademi/kelas_page/deskripsi_page.dart';
import 'package:arkademi/kelas_page/kurikulum_page.dart';
import 'package:arkademi/kelas_page/ulasan_page.dart';
import 'package:arkademi/main_page/FollowerNotchedShape.dart';
import 'package:arkademi/mulai_kelas_page/mulai_kelas_page.dart';
import 'package:arkademi/util/global_var.dart';
import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';
import 'package:shared_preferences/shared_preferences.dart';


class CheckoutPage extends StatefulWidget {
  @override
  _CheckoutPageState createState() => _CheckoutPageState();
}

class _CheckoutPageState extends State<CheckoutPage> {
  int tabIndex = 0;

  bool isUserLogin = false;

  @override
  void initState() {
    super.initState();
    checkUser();
  }

  void checkUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    isUserLogin = prefs.getBool('user_login') ?? false;

    setState(() {
      
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          SingleChildScrollView(
            child: Stack(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 270,
                  color: Color.fromRGBO(221, 221, 221, 1),
                ),
                Image.asset('assets/header-checkout.png'),
                Container(
                  margin: EdgeInsets.only(
                    top: 20,
                  ),
                  child: Stack(
                    children: <Widget>[
                      
                      Container(
                        margin: EdgeInsets.only(
                          top: 12
                        ),
                        width: MediaQuery.of(context).size.width,
                        child: Text("CHECKOUT", 
                        style: TextStyle(
                          
                          fontSize: 18,
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                        ),
                      ),
                      tabIndex != 2 ? Positioned(
                        top: 0,
                        right: 0,
                        child: FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "BATAL",
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w300
                            ),
                          ),
                        ),
                      ) : Container()
                    ],
                  )
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: 70
                  ),
                  padding: EdgeInsets.only(
                    left: 15,
                    right: 15,
                  ),
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      tabIndex != 2 ? Image.asset("assets/banner-kelas.png", width: MediaQuery.of(context).size.width,) : cardTagihan(),
                      
                      
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: 270,
                  ),
                  width: MediaQuery.of(context).size.width,
                  color: Color.fromRGBO(226, 226, 226, 1),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          setState(() {
                            tabIndex = 0;
                          });
                        },
                        child: Container(
                          color: tabIndex == 0 ? Color.fromRGBO(35, 112, 37, 1) : Colors.transparent,
                          child: Column(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                  top: 10,
                                ),
                                width: MediaQuery.of(context).size.width / 3,
                                height: 40,
                                child: Text(
                                  "1. Produk",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300,
                                    color: tabIndex == 0 ? Colors.white : Colors.black,
                                  ),
                                ),
                              ),
                              
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          setState(() {
                            tabIndex = 1;
                          });
                        },
                        child: Container(
                          color: tabIndex == 1 ? Color.fromRGBO(35, 112, 37, 1) : Colors.transparent,
                          child: Column(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                  top: 10,
                                ),
                                width: MediaQuery.of(context).size.width / 3,
                                height: 40,
                                child: Text(
                                  "2.Data Pembeli",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300,
                                    color: tabIndex == 1 ? Colors.white : Colors.black,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          setState(() {
                            tabIndex = 2;
                          });
                        },
                        child: Container(
                          color: tabIndex == 2 ? Color.fromRGBO(35, 112, 37, 1) : Colors.transparent,
                          child: Column(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                  top: 10,
                                ),
                                width: MediaQuery.of(context).size.width / 3,
                                height: 40,
                                child: Text(
                                  "3.Bayar",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300,
                                    color: tabIndex == 2 ? Colors.white : Colors.black,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                      
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: 330,
                  ),
                  padding: EdgeInsets.only(
                    left: 15,
                    right: 15,
                    bottom: 30,
                  ),
                  width: MediaQuery.of(context).size.width,
                  child: content(),
                ),

                
              ],
            ),
          ),
          

          
        ],
      ),

      
    );
  }



  Widget content() {
    if (tabIndex == 0) {
      return tabProduct();
    }else if (tabIndex == 1) {
      return tabDataPembeli();
    } else {
      return tabBayar();
    }
  }

  Widget tabProduct() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Nama Kelas",
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
        ),
        Container(height: 10,),
        Text(
          "Mendirikan Startup & Cara",
          style: TextStyle(
            fontSize: 20,
          ),
        ),
        Container(height: 15,),
        Divider(height: 2,),
        Container(height: 15,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              "Harga",
              style: TextStyle(
                fontSize: 18
              ),
            ),
            Text(
              "Rp 995.000",
              style: TextStyle(
                fontSize: 28,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
        Container(height: 15,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              "Diskon kode unik",
              style: TextStyle(
                fontSize: 18
              ),
            ),
            Text(
              "Rp 450",
              style: TextStyle(
                fontSize: 18
              ),
            ),
          ],
        ),
        Container(height: 15,),
        Container(
          padding: EdgeInsets.only(
            left: 10,
            right: 10,
            top: 10,
            bottom: 10,
          ),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            border: Border.all(
              width: 1,
              color: Color.fromRGBO(217, 217, 217, 1),
            ),
            borderRadius: BorderRadius.circular(3),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Punya kupon?"),
              Container(height: 5,),
              Row(
                children: <Widget>[
                  Expanded(
                    child: TextFormField(
                      decoration: InputDecoration(
                        hintText: "kode kupon",
                        border: InputBorder.none,
                        filled: true,
                        fillColor: Color.fromRGBO(242, 242, 242, 1),
                      ),
                    ),
                  ),
                  Container(width: 3,),
                  SizedBox(
                    height: 42,
                    child: FlatButton(
                      color: Color.fromRGBO(93, 93, 93, 1),
                      onPressed: (){},
                      child: Text(
                        "PAKAI",
                        style: TextStyle(
                          color: Colors.white,
                        ),),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
        Container(height: 15,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              "Diskon kupon",
              style: TextStyle(
                fontSize: 18
              ),
            ),
            Text(
              "Rp 0",
              style: TextStyle(
                fontSize: 18
              ),
            ),
          ],
        ),
        Container(height: 15,),
        Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12.0),
          ),
          elevation: 5,
          child: Container(
            padding: EdgeInsets.only(
              top: 30,
              bottom: 30,
              left: 15,
              right: 15,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              gradient: LinearGradient(
                // Where the linear gradient begins and ends
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                // Add one stop for each color. Stops should increase from 0 to 1
                stops: [0.1, 1.0],
                colors: [
                  // Colors are easy thanks to Flutter's Colors class.
                  Color.fromRGBO(15, 124, 185, 1),
                  Color.fromRGBO(92, 181, 131, 1),
                ],
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Harga",
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.white
                  ),
                ),
                Text(
                  "Rp 995.000",
                  style: TextStyle(
                    fontSize: 28,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        ),

        Container(height: 15,),
        SizedBox(
          width: MediaQuery.of(context).size.width,
          
          child: InkWell(
            onTap: (){
              setState(() {
               tabIndex = 1; 
              });
            },
            child: Card(
              color: Color.fromRGBO(30, 130, 15, 1),
              child: Stack(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.only(
                      top: 15,
                      bottom: 15,
                    ),
                    child: Text(
                      "DATA PEMBELI",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 24,
                        color: Colors.white,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  Positioned(
                    right: 0,
                    top: 5,
                    child: Icon(Icons.chevron_right, size: 50, color: Colors.white,),
                  )
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget tabDataPembeli() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12.0),
          ),
          elevation: 5,
          child: Container(
            padding: EdgeInsets.only(
              top: 15,
              bottom: 20,
              left: 15,
              right: 15,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              gradient: LinearGradient(
                // Where the linear gradient begins and ends
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                // Add one stop for each color. Stops should increase from 0 to 1
                stops: [0.1, 1.0],
                colors: [
                  // Colors are easy thanks to Flutter's Colors class.
                  Color.fromRGBO(15, 124, 185, 1),
                  Color.fromRGBO(92, 181, 131, 1),
                ],
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Nama Kelas",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
                Container(height: 10,),
                Text(
                  "Mendirikan Startup & Cara",
                  maxLines: 2,
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                  ),
                ),
                Container(height: 30,),
                Divider(height: 5, color: Color.fromRGBO(90, 173, 167, 1),),
                Container(height: 10,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Harga akhir",
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.white
                      ),
                    ),
                    Text(
                      "Rp 995.000",
                      style: TextStyle(
                        fontSize: 28,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        Container(height: 30,),
        Container(
          padding: EdgeInsets.only(
            left: 10,
            right: 10,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text(
                    "Nama depan",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.black
                    ),
                  ),
                  Text(
                    "*",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.red
                    ),
                  ),
                  
                ],
              ),
              TextFormField(
                
              ),
              Container(height: 20,),
              Row(
                children: <Widget>[
                  Text(
                    "Nama belakang",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.black
                    ),
                  ),
                  Text(
                    "*",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.red
                    ),
                  ),
                  
                ],
              ),
              TextFormField(
                
              ),
              Container(height: 20,),
              Row(
                children: <Widget>[
                  Text(
                    "Email",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.black
                    ),
                  ),
                  Text(
                    "*",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.red
                    ),
                  ),
                  
                ],
              ),
              TextFormField(
                
              ),

              Container(height: 20,),
              Row(
                children: <Widget>[
                  Text(
                    "Nomor WhatsApp (WA)",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.black
                    ),
                  ),
                  Text(
                    "*",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.red
                    ),
                  ),
                  
                ],
              ),
              TextFormField(
                
              ),
              
            ],
          ),
        ),

        Container(height: 25,),
        SizedBox(
          width: MediaQuery.of(context).size.width,
          
          child: InkWell(
            onTap: (){
              setState(() {
              tabIndex = 2; 
              });
            },
            child: Card(
              color: Color.fromRGBO(255, 90, 0, 1),
              child: Stack(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.only(
                      top: 15,
                      bottom: 15,
                    ),
                    child: Text(
                      "BAYAR",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 24,
                        color: Colors.white,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  Positioned(
                    right: 0,
                    top: 5,
                    child: Icon(Icons.chevron_right, size: 50, color: Colors.white,),
                  )
                ],
              ),
            ),
          ),
        ),
        Container(height: 15,),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Image.asset("assets/mandiri.png"),
            Container( width: 5,),
            Expanded(
              child: Text(
                "Arkademi menerima pembayaran dengan metode transfer ke rekening tujuan Bank Mandiri",
                style: TextStyle(
                  fontSize: 11,
                  fontStyle: FontStyle.italic
                ),
              ),
            )
          ],
        ),
      ]
    );
  }

  Widget cardTagihan() {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
      ),
      elevation: 5,
      child: Container(
        padding: EdgeInsets.only(
          top: 15,
          bottom: 20,
          left: 15,
          right: 15,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          gradient: LinearGradient(
            // Where the linear gradient begins and ends
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            // Add one stop for each color. Stops should increase from 0 to 1
            stops: [0.1, 1.0],
            colors: [
              // Colors are easy thanks to Flutter's Colors class.
              Color.fromRGBO(15, 124, 185, 1),
              Color.fromRGBO(92, 181, 131, 1),
            ],
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Nama Kelas",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
            Container(height: 10,),
            Text(
              "Mendirikan Startup & Cara",
              maxLines: 2,
              style: TextStyle(
                fontSize: 20,
                color: Colors.white,
              ),
            ),
            Container(height: 30,),
            Divider(height: 5, color: Color.fromRGBO(90, 173, 167, 1),),
            Container(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "TAGIHAN",
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.white
                  ),
                ),
                Text(
                  "Rp 995.000",
                  style: TextStyle(
                    fontSize: 28,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget tabBayar() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          children: <Widget>[
            Icon(Icons.hourglass_empty, size: 34,),
            Container(width: 10,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Menunggu Pembayaran",
                  style: TextStyle(
                    fontSize: 24,
                  ),
                ),
                Container(height: 3,),
                Text(
                  "Sisa Waktu Pembayaran",
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
                Container(height: 3,),
                Text(
                  "23:59:59",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                
              ],
            )
          ],
        ),

        Container(height: 20,),
        Divider(height: 5,),
        Container(height: 10,),
        Text(
          "INSTRUKSI PEMBAYARAN",
          style: TextStyle(
            fontSize: 24,
          ),
        ),
        Container(height: 10,),
        Wrap(
          children: <Widget>[
            Text(
              "Transfer pembayaran sesuai tagihan ",
              style: TextStyle(
                fontSize: 16,
              ),
            ),
            Text(
              "sampai digit terakhir ",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold
              ),
            ),
            Text(
              "ke rekening di bawah ini",
              style: TextStyle(
                fontSize: 16,
              ),
            ),

          ],
        ),
        Container(height: 10,),
        Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12.0),
          ),
          elevation: 5,
          child: Container(
            padding: EdgeInsets.only(
              top: 15,
              bottom: 20,
              left: 15,
              right: 15,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              gradient: LinearGradient(
                // Where the linear gradient begins and ends
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                // Add one stop for each color. Stops should increase from 0 to 1
                stops: [0.1, 1.0],
                colors: [
                  // Colors are easy thanks to Flutter's Colors class.
                  Color.fromRGBO(196, 196, 196, 1),
                  Color.fromRGBO(49, 49, 49, 1),
                ],
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Image.asset("assets/mandiri.png", height: 30,),
                Container(height: 5,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Bank",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          "BANK MANDIRI",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 22,
                          ),
                        ),
                        Text(
                          "KCP Pondok Indah, Jakarta Selatan",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        )
                      ],
                    )
                  ],
                ),
                Container(height: 15,),
                Divider(height: 2,color: Colors.white,),
                Container(height: 15,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Nomor\nRekening",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                      ),
                    ),
                    Text(
                      "101-008-000-8251",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 22,
                      ),
                    ),
                  ],
                ),
                Container(height: 15,),
                Divider(height: 2,color: Colors.white,),
                Container(height: 15,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Atas Nama",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                      ),
                    ),
                    Text(
                      "PT Arkademi Daya Indonesia",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      ),
                    ),
                  ],
                ),
                Container(height: 10,),
                
              ],
            ),
          ),
        ),
        Container(height: 20,),
        Text("Batas waktu pembayaran 24 jam atau pemesanan akan dibatlkan secara otomatis"),
        Container(height: 15,),
        Divider(height: 2,color: Colors.grey,),
        Container(height: 15,),
        Text("Tidak diperlukan konfirmasi pembayaran. Verifikasi otomatis 10-15. Kelas akan otomatis terbuka."),
        Container(height: 15,),
        Divider(height: 2,color: Colors.grey,),
        Container(height: 15,),
        Text("Bila dalam 60 menit kelas belum juga terbuka, hubungi email billing@arkademi.com dengan menyertakan bukti pembayaran"),
        Container(height: 15,),
        SizedBox(
          width: MediaQuery.of(context).size.width,
          
          child: InkWell(
            onTap: (){
              Navigator.pop(context);
            },
            child: Card(
              color: Color.fromRGBO(21, 129, 181, 1),
              child: Stack(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.only(
                      top: 15,
                      bottom: 15,
                    ),
                    child: Text(
                      "KEMBALI KE DEPAN",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 24,
                        color: Colors.white,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
