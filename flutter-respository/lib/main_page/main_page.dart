import 'package:arkademi/auth_page/auth_page.dart';
import 'package:arkademi/directory_page/directory_page.dart';
import 'package:arkademi/home_page/home_page.dart';
import 'package:arkademi/home_page/kelas_saya_widget.dart';
import 'package:arkademi/kelas_saya_page/kelas_saya_page.dart';
import 'package:arkademi/notification_page/notification_page.dart';
import 'package:arkademi/profile_page/profile_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'FollowerNotchedShape.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  bool isUserLogin;
  String username = '';
  String avatar = '';
  bool isJoinKelas = true;
  String tokenValue;
  List<Widget> pages = [
    HomePage(
      isUserLogin :false, 
      isJoinKelas : false
    ),
    AuthPage(

    ),
    DirectoryPage(
      
    )
  ];

  final PageStorageBucket bucket = PageStorageBucket();

  int _selectedIndex = 0;

  @override
  void initState() {
    super.initState();
    checkUserLogin();
  }

  void checkUserLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    isUserLogin = prefs.getBool('user_login') ?? false;
    String tokenValue = prefs.getString('token_access');
    String username = prefs.getString('user_name');
    String avatar = prefs.getString('user_avatar');
    print(tokenValue);
    if (isUserLogin == false) {
      pages = [
        HomePage(
           isUserLogin :false, 
          isJoinKelas : false
        ),
        AuthPage(

        ),
        DirectoryPage(
          
        )
      ];
    } else {
      pages = [
        HomePage(
          isUserLogin :true, 
          isJoinKelas : true,
          tokenValue : tokenValue
        ),
        DirectoryPage(

        ),
        KelasSayaPage(

        ),
        NotificationPage(
          
        ),
        ProfilePage(
          isLoginUser: true,
          username: username,
          avatar : avatar,
          tokenValue : tokenValue,
        ),
      ];
    }
 
    setState(() {
      isUserLogin : true;
      isJoinKelas : true;
    });
  }

  @override
  Widget build(BuildContext context) {

    ;

    return Scaffold(
      body: PageStorage(
        child: pages[_selectedIndex],
        bucket: bucket,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => isUserLogin ? changeSelected(2) : changeSelected(1),
        tooltip: 'Increment',
        backgroundColor: Colors.orange[700],
        child: isUserLogin ? SvgPicture.asset(
            "assets/book-reader.svg", 
            height: 30, 
            width: 30,
            color: Colors.white,
          ) : Icon(Icons.person, size: 40,),
      ),
      bottomNavigationBar: isUserLogin ? bottomAppBarLogin() : bottomAppBarNoLogin(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  Widget bottomAppBarLogin() {
    return BottomAppBar(
      color: _selectedIndex == 2 ? Colors.yellow : Colors.white,
      shape: FollowerNotchedShape(inverted: true),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          InkWell(
            onTap: () {
              changeSelected(0);
            },
            child: Container(
              decoration: BoxDecoration(
                color: _selectedIndex == 0 ? Colors.yellow : Colors.white,
              ),
              height: 60,
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              width: MediaQuery.of(context).size.width / 4 - 20,
              child: Column(
                children: <Widget>[
                  SvgPicture.asset(
                    "assets/home.svg", 
                    height: 20, 
                    width: 20,
                    color: Colors.grey[700],
                  ),
                  Container(height: 5,),
                  Text("Home"),
                ],
              ),
            ),
          ),
          InkWell(
            onTap: () {
              changeSelected(1);
            },
            child: Container(
              decoration: BoxDecoration(
                color: _selectedIndex == 1 ? Colors.yellow : Colors.white,
              ),
              height: 60,
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              width: MediaQuery.of(context).size.width / 4 - 20,
              child: Column(
                children: <Widget>[
                  SvgPicture.asset(
                    "assets/bars.svg", 
                    height: 20, 
                    width: 20,
                    color: Colors.grey[700],
                  ),
                  Container(height: 5,),
                  Text("Directory"),
                ],
              ),
            ),
          ),
          
          InkWell(
            onTap: () {
              changeSelected(2);
            },
            child:Container(
              height: 50,
              width: 80,
              // color: Colors.red,
              padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
              child: Text("Kelas Saya", textAlign: TextAlign.center,),
            ),
          ),
          
          
          InkWell(
            onTap: () {
              changeSelected(3);
            },
            child: Container(
              decoration: BoxDecoration(
                color: _selectedIndex == 3 ? Colors.yellow : Colors.white,
              ),
              height: 60,
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              width: MediaQuery.of(context).size.width / 4 - 20,
              child: Column(
                children: <Widget>[
                  SvgPicture.asset(
                    "assets/bell.svg", 
                    height: 20, 
                    width: 20,
                    color: Colors.grey[700],
                  ),
                  Container(height: 5,),
                  Text("Notifikasi"),
                ],
              ),
            ),
          ),

          InkWell(
            onTap: () {
              changeSelected(4);
            },
            child: Container(
              decoration: BoxDecoration(
                color: _selectedIndex == 4 ? Colors.yellow : Colors.white,
              ),
              height: 60,
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              width: MediaQuery.of(context).size.width / 4 - 20,
              child: Column(
                children: <Widget>[
                  SvgPicture.asset(
                    "assets/user.svg", 
                    height: 20, 
                    width: 20,
                    color: Colors.grey[700],
                  ),
                  Container(height: 5,),
                  Text("Profil"),
                ],
              ),
            ),
          ),
          
          
        ],
      ),
      

    );
  }

  Widget bottomAppBarNoLogin() {
    return BottomAppBar(
      color: _selectedIndex == 1 ? Colors.yellow : Colors.white,
      shape: FollowerNotchedShape(inverted: true),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          InkWell(
            onTap: () {
              changeSelected(0);
            },
            child: Container(
              decoration: BoxDecoration(
                color: _selectedIndex == 0 ? Colors.yellow : Colors.white,
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(100)
                )
              ),
              height: 60,
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              width: MediaQuery.of(context).size.width / 2 - 40,
              child: Column(
                children: <Widget>[
                  SvgPicture.asset(
                    "assets/home.svg", 
                    height: 20, 
                    width: 20,
                    color: Colors.grey[700],
                  ),
                  Container(height: 5,),
                  Text("Home"),
                ],
              ),
            ),
          ),
          
          InkWell(
            onTap: () {
              changeSelected(1);
            },
            child:Container(
              height: 50,
              width: 80,
              // color: Colors.red,
              padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
              child: Text("LOGIN", textAlign: TextAlign.center,),
            ),
          ),
          
          InkWell(
            onTap: () {
              changeSelected(2);
            },
            child: Container(
              decoration: BoxDecoration(
                color: _selectedIndex == 2 ? Colors.yellow : Colors.white,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(100)
                )
              ),
              height: 60,
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              width: MediaQuery.of(context).size.width / 2 - 40,
              child: Column(
                children: <Widget>[
                  SvgPicture.asset(
                    "assets/bars.svg", 
                    height: 20, 
                    width: 20,
                    color: Colors.grey[700],
                  ),
                  Container(height: 5,),
                  Text("Direktori"),
                ],
              ),
            ),
          ),
          
          
        ],
      ),
      

    );
  }
  void changeSelected(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}