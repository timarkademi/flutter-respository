import 'package:arkademi/kelas_page/kelas_page.dart';

import 'category_widget.dart';
import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';

import 'directory_list_widget.dart';

class DirectoryPage extends StatefulWidget {
  @override
  _DirectoryPageState createState() => _DirectoryPageState();
}

class _DirectoryPageState extends State<DirectoryPage> {
  List<Widget> list = [];

  int currentIndex = 0;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => init());
  }

  void init() {
    print("test");
    list = [
      directoryWidget(),
      KelasPage(),
    ];
    setState(() {
      
    });
  }


  @override
  Widget build(BuildContext context) {
    return list.length == 0 ? Container(
      child: CircularProgressIndicator(),
    ) : list[currentIndex];
  }

  Widget directoryWidget() {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(
            bottom: 50,
          ),
          child: Column(
            children: <Widget>[
              _headerWidget(),
            ]
          ),
        ),
      ),
    );
  }

  Widget _headerWidget() {
    return Stack(
      children: <Widget>[
        Image.asset('assets/header.png'),
        Container(
          margin: EdgeInsets.only(
            top: 10,
          ),
          child: IconButton(
              onPressed: (){},
              icon: Icon(Mdi.chevronLeft, size: 40,color: Colors.white,),
            ),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.only(
            top: 30,
          ),
          child: Text("DIREKTORI", style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
          textAlign: TextAlign.center,
          )
        ),
        Container(
          margin: EdgeInsets.only(
            top: 70,
          ),
          padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
          child: TextField(
            decoration: InputDecoration(
              filled: true,
              fillColor: Color.fromRGBO(0, 0, 0, 0.1),

              
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
                borderSide: BorderSide(
                  color: Colors.white
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
                borderSide: BorderSide(
                  color: Colors.white
                ),
              ),
              contentPadding: EdgeInsets.only(
                top: 5,
                bottom: 5,
                left: 20,
                right: 10
              ),
              hintText: "Mau belajar apa hari ini?",
              hintStyle: TextStyle(
                color: Colors.white,
              ),
              suffix: Icon(Mdi.magnify, color: Colors.white,),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            top: 95
          ),
          child: Column(
            children: <Widget>[
               CategoryWidget(),
               Container(height: 10,),
               DirectoryListWidget(),
            ],
          ),
        ),
      ]
    );
  }
}

