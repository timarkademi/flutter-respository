import 'package:arkademi/auth_page/auth_page.dart';
import 'package:arkademi/auth_page/login_page.dart';
import 'package:arkademi/checkout_page/checkout_page.dart';
import 'package:arkademi/data/ulasan.dart' as ulasan;
import 'package:arkademi/kelas_page/deskripsi_page.dart';
import 'package:arkademi/kelas_page/kurikulum_page.dart';
import 'package:arkademi/kelas_page/ulasan_page.dart';
import 'package:arkademi/main_page/FollowerNotchedShape.dart';
import 'package:arkademi/mulai_kelas_page/mulai_kelas_page.dart';
import 'package:arkademi/util/global_var.dart';
import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';



class KelasPage extends StatefulWidget {

  List dataJSON;
  final bool isJoinKelas;
  final bool isUserLogin;
  final int index;
  String tokenValue;


  KelasPage({
    this.isJoinKelas,
    this.index,
    this.dataJSON,
    this.isUserLogin,
    this.tokenValue
});

  @override
  _KelasPageState createState() => _KelasPageState(
    isJoinKelas: isJoinKelas,
    isUserLogin : isUserLogin,
    tokenValue : tokenValue
  );

  
}

class _KelasPageState extends State<KelasPage> {

  
  int tabIndex = 0;
  bool isJoinKelas;
  bool isUserLogin;
  int index = 0;
  int id = 0;
  String tokenValue;


  _KelasPageState({
    this.isJoinKelas,
    this.index,
    this.isUserLogin,
    this.tokenValue
  });

  List selectDATA;

  Future<List> fetchPost() async {
    final response = await http.get(
      'https://staging.arkademi.com/wp-json/wplms/v1/course/${widget.dataJSON[widget.index]['data']['id']}',
      headers: {"Accept": "application/json"},
    );

    this.setState(() {
      selectDATA = json.decode(response.body);
    });
  }

  @override
  void initState() {
    super.initState();
    fetchPost();
    isUserLogin;
    isJoinKelas;
    tokenValue;
  }

  @override
  Widget build(BuildContext context) {
   // print("Rifki + ${tokenValue}");
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          SingleChildScrollView(
            child: Stack(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 395,
                  color: Colors.blueGrey[700],
                ),
                Image.asset('assets/header.png'),
                Container(
                  margin: EdgeInsets.only(
                    top: 20,
                  ),
                  child: Stack(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(
                          top: 12
                        ),
                        width: MediaQuery.of(context).size.width,
                        child: Text("KELAS", style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                        ),
                      ),
                      IconButton(
                        onPressed: () => Navigator.pop(context),
                        icon: Icon(Icons.chevron_left, color: Colors.white, size: 34,),
                      ),
                    ],
                  )
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: 70
                  ),
                  padding: EdgeInsets.only(
                    left: 15,
                    right: 15,
                  ),
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Image.network(widget.dataJSON[widget.index]['data']['featured_image'], width: MediaQuery.of(context).size.width,),
                      Text(widget.dataJSON[widget.index]['data']['name'], style: TextStyle(
                        fontSize: 22,
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.start,
                      ),
                      Container(height: 8,),
                      Text(widget.dataJSON[widget.index]['data']['instructor']['name'], style: TextStyle(
                        fontSize: 12,
                        color: Colors.yellow,
                      ),
                      textAlign: TextAlign.start,
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          top: 10,
                        ),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: 22,
                                margin: EdgeInsets.only(
                                  right: 5,
                                ),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(10),
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                    bottomLeft: Radius.circular(10)
                                  ),
                                  color: Colors.blue
                                ),
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(100),
                                          bottomLeft: Radius.circular(50),
                                          topLeft: Radius.circular(50)
                                        ),
                                        color: Colors.yellow
                                      ),
                                      height: 22,
                                      width: 40,
                                      child: Icon(Icons.people, size: 16,)
                                    ),
                                    Text( "${widget.dataJSON[widget.index]['data']['total_students']} siswa", style: TextStyle(
                                      fontSize: 10,
                                      color: Colors.white,
                                    ),),
                                  ],
                                )
                              ),
                            ),
                            Expanded(
                              child: Container(
                                height: 22,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(10),
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                    bottomLeft: Radius.circular(10)
                                  ),
                                  color: Colors.blue
                                ),
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(100),
                                          bottomLeft: Radius.circular(50),
                                          topLeft: Radius.circular(50)
                                        ),
                                        color: Colors.yellow
                                      ),
                                      height: 22,
                                      width: 40,
                                      child: Container(
                                        padding: EdgeInsets.only(
                                          left: 8,
                                          top: 5,
                                        ),
                                        child: Text(
                                          "4.8",
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 11,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Text(
                                        "${widget.dataJSON[widget.index]['data']['total_students']} siswa", 
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              ),
                            ),
                            Expanded(
                              child: Container(
                                height: 22,
                                margin: EdgeInsets.only(
                                  left: 5,
                                ),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(10),
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                    bottomLeft: Radius.circular(10)
                                  ),
                                  color: Colors.blue
                                ),
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(100),
                                          bottomLeft: Radius.circular(50),
                                          topLeft: Radius.circular(50)
                                        ),
                                        color: Colors.yellow
                                      ),
                                      height: 22,
                                      width: 40,
                                      child: Icon(Mdi.thumbUp, size: 16,)
                                    ),
                                    Expanded(
                                      child: Text(
                                        "0 ulasan", 
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: 360,
                  ),
                  width: MediaQuery.of(context).size.width,
                  color: Colors.grey[300],
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          setState(() {
                            tabIndex = 0;
                          });
                        },
                        child: Container(
                          color: tabIndex == 0 ? Colors.brown : Colors.transparent,
                          child: Column(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                  top: 10,
                                ),
                                width: MediaQuery.of(context).size.width / 3,
                                height: 40,
                                child: Text(
                                  "Deskripsi",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300,
                                    color: tabIndex == 0 ? Colors.white : Colors.black,
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width / 3,
                                height: 3,
                                color: tabIndex == 0 ? Colors.orange : Colors.grey,
                              ),
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          setState(() {
                            tabIndex = 1;
                          });
                        },
                        child: Container(
                          color: tabIndex == 1 ? Colors.brown : Colors.transparent,
                          child: Column(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                  top: 10,
                                ),
                                width: MediaQuery.of(context).size.width / 3,
                                height: 40,
                                child: Text(
                                  "Kurikulum",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300,
                                    color: tabIndex == 1 ? Colors.white : Colors.black,
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width / 3,
                                height: 3,
                                color: tabIndex == 1 ? Colors.orange : Colors.grey,
                              ),
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          setState(() {
                            tabIndex = 2;
                          });
                        },
                        child: Container(
                          color: tabIndex == 2 ? Colors.brown : Colors.transparent,
                          child: Column(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                  top: 10,
                                ),
                                width: MediaQuery.of(context).size.width / 3,
                                height: 40,
                                child: Text(
                                  "Ulasan",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300,
                                    color: tabIndex == 2 ? Colors.white : Colors.black,
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width / 3,
                                height: 3,
                                color: tabIndex == 2 ? Colors.orange : Colors.grey,
                              ),
                            ],
                          ),
                        ),
                      )
                      
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: 420,
                  ),
                  padding: EdgeInsets.only(
                    left: 15,
                    right: 15,
                    bottom: 30,
                  ),
                  width: MediaQuery.of(context).size.width,
                  child: content(),
                ),

              ],
            ),
          ),
          Positioned(
            bottom: 50,
            // left: MediaQuery.of(context).size.width / 2 - 150,
            left: 30,
            right: 30,
            child: InkWell(
              onTap: (){

              },
              child: joinWidget(95000, 500000, isJoinKelas, isUserLogin),
            ),
          ),
        ],
      ),

      floatingActionButton: FloatingActionButton(
        onPressed: () => changeSelected(1),
        tooltip: 'Increment',
        backgroundColor: Colors.orange[700],
        child: Icon(Icons.widgets, size: 40,),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: isUserLogin ?  BottomAppBarLogin() : BottomAppBarNoLogin() 
    );
  }

  Widget BottomAppBarNoLogin(){
    return BottomAppBar(
        color: _selectedIndex == 1 ? Colors.yellow : Colors.white,
        shape: FollowerNotchedShape(inverted: true),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            InkWell(
              onTap: () {
                changeSelected(0);
              },
              child: Container(
                decoration: BoxDecoration(
                  color: _selectedIndex == 0 ? Colors.white : Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(100)
                  )
                ),
                height: 60,
                padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                width: MediaQuery.of(context).size.width / 2 - 40,
                child: Column(
                  children: <Widget>[
                    Icon(Icons.home),
                    Text("Home"),
                  ],
                ),
              ),
            ),

            InkWell(
              onTap: () {
                changeSelected(1);
              },
              child:Container(
                height: 50,
                width: 80,
                // color: Colors.red,
                padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                child: Text("LOGIN", textAlign: TextAlign.center,),
              ),
            ),

            InkWell(
              onTap: () {
                changeSelected(2);
              },
              child: Container(
                decoration: BoxDecoration(
                  color: _selectedIndex == 2 ? Colors.yellow : Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(100)
                  )
                ),
                height: 60,
                padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                width: MediaQuery.of(context).size.width / 2 - 40,
                child: Column(
                  children: <Widget>[
                    Icon(Icons.menu),
                    Text("Direktori"),
                  ],
                ),
              ),
            ),

          ],
        ),
      );
  }

  Widget BottomAppBarLogin(){
     return BottomAppBar(
      color: _selectedIndex == 2 ? Colors.yellow : Colors.white,
      shape: FollowerNotchedShape(inverted: true),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          InkWell(
            onTap: () {
              changeSelected(0);
            },
            child: Container(
              decoration: BoxDecoration(
                color: _selectedIndex == 0 ? Colors.yellow : Colors.white,
              ),
              height: 60,
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              width: MediaQuery.of(context).size.width / 4 - 20,
              child: Column(
                children: <Widget>[
                  SvgPicture.asset(
                    "assets/home.svg", 
                    height: 20, 
                    width: 20,
                    color: Colors.grey[700],
                  ),
                  Container(height: 5,),
                  Text("Home"),
                ],
              ),
            ),
          ),
          InkWell(
            onTap: () {
              changeSelected(1);
            },
            child: Container(
              decoration: BoxDecoration(
                color: _selectedIndex == 1 ? Colors.yellow : Colors.white,
              ),
              height: 60,
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              width: MediaQuery.of(context).size.width / 4 - 20,
              child: Column(
                children: <Widget>[
                  SvgPicture.asset(
                    "assets/bars.svg", 
                    height: 20, 
                    width: 20,
                    color: Colors.grey[700],
                  ),
                  Container(height: 5,),
                  Text("Directory"),
                ],
              ),
            ),
          ),

          InkWell(
            onTap: () {
              changeSelected(2);
            },
            child:Container(
              height: 50,
              width: 80,
              // color: Colors.red,
              padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
              child: Text("Kelas Saya", textAlign: TextAlign.center,),
            ),
          ),

          InkWell(
            onTap: () {
              changeSelected(3);
            },
            child: Container(
              decoration: BoxDecoration(
                color: _selectedIndex == 3 ? Colors.yellow : Colors.white,
              ),
              height: 60,
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              width: MediaQuery.of(context).size.width / 4 - 20,
              child: Column(
                children: <Widget>[
                  SvgPicture.asset(
                    "assets/bell.svg", 
                    height: 20, 
                    width: 20,
                    color: Colors.grey[700],
                  ),
                  Container(height: 5,),
                  Text("Notifikasi"),
                ],
              ),
            ),
          ),

          InkWell(
            onTap: () {
              changeSelected(4);
            },
            child: Container(
              decoration: BoxDecoration(
                color: _selectedIndex == 4 ? Colors.yellow : Colors.white,
              ),
              height: 60,
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              width: MediaQuery.of(context).size.width / 4 - 20,
              child: Column(
                children: <Widget>[
                  SvgPicture.asset(
                    "assets/user.svg", 
                    height: 20, 
                    width: 20,
                    color: Colors.grey[700],
                  ),
                  Container(height: 5,),
                  Text("Profil"),
                ],
              ),
            ),
          ),

        ],
      ),

    );


  }

  int _selectedIndex = 0;  
  void changeSelected(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  void _mulaiKelas() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MulaiKelasPage(widget.dataJSON[widget.index]['data']['id'],selectDATA,1,tokenValue))
    );
  }

  void _bergabung() {
    if(widget.dataJSON[widget.index]['data']['price'] == 0){

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => AuthPage()),
    );

    }else{

    if(isUserLogin == true){
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => CheckoutPage()),
    );

    }else{
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => AuthPage()),
    );
    }
    }
  }

  Widget joinWidget(int hargaAsli, int hargaDiskon, isJoinKelas, isUserLogin) {
    return InkWell(
      onTap: isJoinKelas == true && isUserLogin == true ? _mulaiKelas : _bergabung,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25),
          color: isJoinKelas == true ? Colors.green : Colors.amber[900],
        ),
        height: 50,
        width: MediaQuery.of(context).size.width,
        
        child: isJoinKelas == true ? Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Container(
  
               // child : selectDATA[0]['data']['course']['user_status'] == "" ?  Text(
               //   "MULAI KELAS",
                //  style: TextStyle(
               //     fontSize: 20,
               //     color: Colors.white
               //   ),
               //   textAlign: TextAlign.center,
               // ) : 
              child :Text(
                  "LANJUTKAN",
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.white
                  ),
                  textAlign: TextAlign.center,
                ),



              ),
            ),
          ],
        ) : Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Container(
                child: Text(
                  "BERGABUNG",
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.white
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(
                  right: 10,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(20),
                    bottomRight: Radius.circular(20),
                    bottomLeft: Radius.circular(20)
                  ),
                  color: Colors.deepOrange[300],
                ),
                width: MediaQuery.of(context).size.width / 2,
                height: 50,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      "${widget.dataJSON[widget.index]['data']['price'] == 0 ? "Gratis" : widget.dataJSON[widget.index]['data']['price']}" ,
                      style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.right,
                    ),
                    Text(
                      "${widget.dataJSON[widget.index]['data']['price'] == 0 ? "Gratis" : widget.dataJSON[widget.index]['data']['price']}" ,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 12,
                      ),
                      textAlign: TextAlign.right,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget content() {
    if (tabIndex == 0) {
      return DeskripsiPage(
        title: widget.dataJSON[widget.index]['data']['name'],
        content: widget.dataJSON[widget.index]['data']['description'],
        //content: selectDATA[0]['data']['post_content']['rest_content'],
      );
    }else if (tabIndex == 1) {
      return KurikulumPage(curriculum : selectDATA[0]['data']['curriculum']);
    } else {
      return UlasanPage(ulasanList : selectDATA[0]['data']['reviews']);
    }
  }

}
