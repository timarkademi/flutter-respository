import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:mdi/mdi.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart';


class KurikulumPage extends StatefulWidget {
  final List curriculum;

  KurikulumPage({
    this.curriculum,
  });
  @override
  _KurikulumPageState createState() =>
      _KurikulumPageState(curriculum: curriculum);
}

class _KurikulumPageState extends State<KurikulumPage> {
  List curriculum;
  List<Widget> curriculumWidgetList = [];

  _KurikulumPageState({this.curriculum});

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) => curriculumWidget());
  }

  void curriculumWidget() {
    curriculumWidgetList.clear();

    for (int i = 0; i < curriculum.length; i++) {
      curriculumWidgetList.add(
        Container(
          margin: EdgeInsets.only(
            bottom: 20,
          ),
          child: InkWell(
            onTap: () {},
            child: Card(
              elevation: 5,
              color: Colors.grey[100],
              child: Container(
                padding: EdgeInsets.only(
                  left: 10,
                  right: 10,
                  bottom: 15,
                ),
                child: Column(
                  children: <Widget>[
                    new ListTile(
                      title: Html ( data : curriculum[i]['title']),
                      leading: new Icon(
                        Icons.play_circle_outline,
                        color: Colors.red,
                      ),
                      subtitle: Html ( data : "Duration : ${curriculum[i]['duration']}"),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: curriculumWidgetList,
      ),
    );
  }
}
