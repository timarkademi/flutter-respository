import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart';
import 'package:url_launcher/url_launcher.dart';


class DeskripsiPage extends StatefulWidget {
  final String title;
  final String content;

  DeskripsiPage({
    this.title,
    this.content,
  });
  @override
  _DeskripsiPageState createState() => _DeskripsiPageState(
    title: title,
    content: content,
  );
}

class _DeskripsiPageState extends State<DeskripsiPage> {
  String title;
  String content;
  
  _DeskripsiPageState({
    this.title,
    this.content,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Html(
            data : title
          ),
          Container(height: 10,),
          Html(
            data : content,
          ),
        ],
      ),
    );
  }
}