import 'package:arkademi/kelas_page/kelas_page.dart';
import 'package:arkademi/main_page/FollowerNotchedShape.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'kuis_question_page.dart';

class KuisPage extends StatefulWidget {
  @override
  _KuisPageState createState() => _KuisPageState();
}

class _KuisPageState extends State<KuisPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            headerWidget(),
            Container(
              width: MediaQuery.of(context).size.width - 60,
              padding: EdgeInsets.only(
                left: 10,
                right: 10,
                top: 10,
                bottom: 10,
              ),
              decoration: BoxDecoration(
                color: Color.fromRGBO(242, 99, 99, 1),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Row(
                children: <Widget>[
                  Icon(Icons.info, color: Colors.white,),
                  Container(width: 10,),
                  new Flexible(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(
                            "Sebagian kuis menentukan anda. Mulai kuis bila anda sudah benar-benar siap. Jangan meninggalkan kuis saat dikerjakan",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 14,
                            ),
                          )
                      ],
                    ),
                  ),
                ],
              ),
            ),

            Card(
              elevation: 5,
              margin: EdgeInsets.only(
                top: 20,
              ),
              child: Container(
                width: MediaQuery.of(context).size.width - 60,
                padding: EdgeInsets.only(
                  left: 15,
                  right: 15,
                  top: 15,
                  bottom: 15,
                ),
                child:Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                        ),
                      )
                    ],
                  ),
              ),
            ),
            Container(height: 50,),
          ]
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => KuisQuestionPage()),
          );
        },
        tooltip: 'Increment',
        backgroundColor: Colors.transparent,
        elevation: 0,
        child: SvgPicture.asset("assets/play-circle.svg", height: 50,),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        color: Colors.white,
        shape: FollowerNotchedShape(inverted: true),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            InkWell(
              onTap: () {

              },
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(100)
                  )
                ),
                height: 60,
                padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                width: MediaQuery.of(context).size.width / 4 - 20,
                child: Column(
                  children: <Widget>[
                    SvgPicture.asset("assets/angle-left.svg", height: 30,),
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => KelasPage()),
                );
              },
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(100)
                  )
                ),
                height: 60,
                padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                width: MediaQuery.of(context).size.width / 4 - 20,
                child: Column(
                  children: <Widget>[
                    SvgPicture.asset("assets/book-open.svg", height: 25,),
                    Text("Lobi"),
                  ],
                ),
              ),
            ),
            
            InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => KuisQuestionPage()),
                );
              },
              child:Container(
                height: 50,
                width: 80,
                // color: Colors.red,
                padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                child: Text("Mulai", textAlign: TextAlign.center,),
              ),
            ),

            
            InkWell(
              onTap: () {

              },
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(100)
                  )
                ),
                height: 60,
                padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                width: MediaQuery.of(context).size.width / 4 - 20,
                child: Column(
                  children: <Widget>[
                    SvgPicture.asset("assets/indent.svg", height: 25,),
                    Text("Kurikulum"),
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () {
                
              },
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(100)
                  )
                ),
                height: 60,
                padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                width: MediaQuery.of(context).size.width / 4 - 20,
                
              ),
            ),
            
            
          ],
        ),
        

      )
    );
  }

  Widget headerWidget() {
    return Stack(
      children: <Widget>[
        Image.asset('assets/profile-header.png'),
        Container(
          margin: EdgeInsets.only(
            top: 20,
          ),
          child: Stack(
            children: <Widget>[
              
              Container(
                margin: EdgeInsets.only(
                  top: 12
                ),
                width: MediaQuery.of(context).size.width,
                child: Text("KUIS", style: TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 65
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Text(
                          "Mendirikan Startup & Cara Membagi Saham",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            top: 10,
                          ),
                          child: Text(
                            "Kuis Mendirikan Startup",
                            style: TextStyle(
                              fontSize: 24,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        Row(
                          children: <Widget>[
                            Stack(
                              children: <Widget>[
                                Image.asset("assets/rectangle.png", fit: BoxFit.cover, height: 100,),
                                Positioned(
                                  top: 20,
                                  left: 0,
                                  right: 0,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        "JUMLAH SOAL",
                                        style: TextStyle(
                                          fontSize: 11,
                                        ),
                                      ),
                                      Text(
                                        "10",
                                        style: TextStyle(
                                          fontSize: 30,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                            Stack(
                              children: <Widget>[
                                Image.asset("assets/rectangle.png", fit: BoxFit.cover, height: 100,),
                                Positioned(
                                  top: 20,
                                  left: 0,
                                  right: 0,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        "WAKTU (menit)",
                                        style: TextStyle(
                                          fontSize: 11,
                                        ),
                                      ),
                                      Text(
                                        "10:00",
                                        style: TextStyle(
                                          fontSize: 30,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                        
                        
                        
                      ],
                    ),
                  ],
                ),
              ),
              
            ],
          )
        ),
      ],
    );
  }


}