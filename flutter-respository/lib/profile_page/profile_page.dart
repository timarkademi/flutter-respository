import 'package:arkademi/aktivitas_page/aktivitas_page.dart';
import 'package:arkademi/edit_profile_page/edit_profile_page.dart';
import 'package:arkademi/nilai_kuis_page/nilai_kuis_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;


class ProfilePage extends StatefulWidget {

  final bool isLoginUser;
  final String username; 
  final String avatar;
  final String tokenValue;
  ProfilePage({
    this.isLoginUser,
    this.username,
    this.avatar,
    this.tokenValue,
  });

  

  @override
  _ProfilePageState createState() => _ProfilePageState(
    avatar : avatar,
    isLoginUser:isLoginUser,
    username:username,
    tokenValue: tokenValue
  );
}

class _ProfilePageState extends State<ProfilePage> {


  bool isLoginUser = false;
  String username = '';
  String avatar = '';
  String tokenValue;

  _ProfilePageState({
    this.isLoginUser,
    this.username,
    this.avatar,
    this.tokenValue
  });


  @override
  void initState() {
    super.initState();
    this.avatar;
    this.username;
    this.isLoginUser;
    this.tokenValue;
  }


  @override
  Widget build(BuildContext context) {


    

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            headerWidget(),
            listTileBlue(
              "assets/user-edit.svg",
              "Edit Profil",
              () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => EditProfilePage()),
                );
              }
            ),
            Container(
              padding: EdgeInsets.only(
                top: 15,
                bottom: 15,
                left: 30
              ),
              child: Text(
                "DASHBOARD",
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            listTileBlue(
              "assets/book-reader.svg",
              "Kelas Saya",
              () {
                print("KLIK MENU");
              }
            ),
            listTileBlue(
              "assets/medal.svg",
              "Nilai Kuis",
              () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => NilaiKuisPage()),
                );
              }
            ),
            listTileBlue(
              "assets/aktivitas.svg",
              "Aktivitas",
              () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AktivitasPage()),
                );
              }
            ),
            Container(height: 40,),
            listTileRed(
              "assets/power-off.svg",
              "Keluar",
              _logout
            ),
            Container(height: 40,),
          ],
        ),
      ),
    );
  }

  Widget listTileBlue(String svg, String title, VoidCallback onTap) {
    return InkWell(
      onTap: onTap,
      child: Container(
        color: Color.fromRGBO(239, 247, 254, 1),
        margin: EdgeInsets.only(
          bottom: 3,
        ),
        padding: EdgeInsets.only(
          top: 3,
          bottom: 3,
          left: 15,
          right: 5,
        ),
        child: ListTile(
          leading: SvgPicture.asset(
            svg,
            height: 25,
            color: Color.fromRGBO(119, 184, 226, 1),
          ),
          title: Text(
            title,
            style: TextStyle(
              fontSize: 20
            ),
          ),
          trailing: Icon(Icons.chevron_right, color:  Color.fromRGBO(119, 184, 226, 1), size: 40,),
        ),
      ),
    );
  }
  Widget listTileRed(String svg, String title, VoidCallback onTap) {
    return InkWell(
      onTap: onTap,
      child: Container(
        color: Color.fromRGBO(252, 231, 231, 1),
        margin: EdgeInsets.only(
          bottom: 3,
        ),
        padding: EdgeInsets.only(
          top: 3,
          bottom: 3,
          left: 15,
          right: 5,
        ),
        child: ListTile(
          leading: SvgPicture.asset(
            svg,
            height: 25,
            color: Color.fromRGBO(146, 2, 2, 1),
          ),
          title: Text(
            title,
            style: TextStyle(
              fontSize: 20
            ),
          ),
          trailing: Icon(Icons.chevron_right, color:  Color.fromRGBO(146, 2, 2, 1), size: 40,),
        ),
      ),
    );
  }

  Widget headerWidget() {
    return Stack(
      children: <Widget>[
        Image.asset('assets/profile-header.png'),
        Container(
          margin: EdgeInsets.only(
            top: 20,
          ),
          child: Stack(
            children: <Widget>[
              
              Container(
                margin: EdgeInsets.only(
                  top: 12
                ),
                width: MediaQuery.of(context).size.width,
                child: Text("PROFIL", style: TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 65
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Image.network(avatar, width: 150,),
                        Text(
                          username,
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                        Text(
                          "siswa",
                          style: TextStyle(
                            fontSize: 11,
                          ),
                        ),
                        Container(height: 30,),
                        
                      ],
                    ),
                  ],
                ),
              ),
              IconButton(
                onPressed: () => Navigator.pop(context),
                icon: Icon(Icons.chevron_left, color: Colors.white, size: 34,),
              ),
            ],
          )
        ),
      ],
    );
  }

  void _logout() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
   

 
final response = await http.post(
        "https://staging.arkademi.com/wp-json/wplms/v1/user/logout",
        headers: {
          "Content-Type": "application/json",
          "Authorization": tokenValue //ini token
          });
        print(response.body);
  

    if(response.body == "null" ){

    print('logout');

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('user_login', false) ;
    Navigator.of(context)
    .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
    

    }else{
      print("gagal");
    }


  }
}