import 'package:arkademi/auth_page/auth_page.dart';
import 'package:arkademi/auth_page/login_page.dart';
import 'package:arkademi/data/ulasan.dart' as ulasan;
import 'package:arkademi/kelas_page/deskripsi_page.dart';
import 'package:arkademi/kelas_page/kurikulum_page.dart';
import 'package:arkademi/kelas_page/ulasan_page.dart';
import 'package:arkademi/kuis_page/kuis_page.dart';
import 'package:arkademi/main_page/FollowerNotchedShape.dart';
import 'package:arkademi/util/global_var.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mdi/mdi.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'package:flutter_simple_video_player/flutter_simple_video_player.dart';

class MulaiKelasPage extends StatefulWidget {
  final idCource;
  List selectDATA;
  int ids ;
  String tokenValue;
  
  MulaiKelasPage(this.idCource, this.selectDATA, this.ids, this.tokenValue);

  @override
  _MulaiKelasPageState createState() => _MulaiKelasPageState(
        idCource: idCource,
        selectDATA : selectDATA,
        ids:ids,
        tokenValue: tokenValue,
        );
      
}

class _MulaiKelasPageState extends State<MulaiKelasPage> {

  List pilihDATA = [];
  int idCource;
  int ids;
  List selectDATA = [];
  String tokenValue;
  List ambil = [];
  _MulaiKelasPageState({this.idCource, this.selectDATA,this.ids,this.tokenValue,this.pilihDATA});

  Future<List> fetchPost() async {
    final response = await http.get(
      'https://staging.arkademi.com/wp-json/wplms/v1/user/coursestatus/${idCource}/item/${widget.selectDATA[0]['data']['curriculum'][widget.ids]['id']}' ,
      headers: {"Authorization":"%8ZQGXjVv!$KpvVMkuyFjnR%ORrX*@U6(MXZxGUH","Accept": "application/json"},);
      Map<String, dynamic> map = json.decode(response.body);
      List<dynamic> pilihDATA = map[0]['meta'];
        this.setState(() {
            ambil = pilihDATA;
        });

        
    }

  int tabIndex = 0;
  bool isUserLogin = true;
  @override
  void initState() {
    super.initState();
    checkUser();
    fetchPost();
    selectDATA;
    tokenValue;
  }

  void checkUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    isUserLogin = prefs.getBool('user_login') ?? true;
    this.setState(() {
        isUserLogin;
    });
  }

  @override
  Widget build(BuildContext context) {
      return Scaffold(
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            SingleChildScrollView(
              child: Stack(
                children: <Widget>[
                  Image.asset(
                    'assets/appbar.png',
                    height: 70,
                    fit: BoxFit.cover,
                  ),
                  Container(
                      margin: EdgeInsets.only(
                        top: 20,
                      ),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(top: 12),
                            width: MediaQuery.of(context).size.width,
                            child: Text(
                              "SESI AJAR",
                              style: TextStyle(
                                fontSize: 18,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          IconButton(
                            onPressed: () => Navigator.pop(context),
                            icon: Icon(
                              Icons.chevron_left,
                              color: Colors.white,
                              size: 34,
                            ),
                          ),
                        ],
                      )),
                  Container(
                    margin: EdgeInsets.only(top: 70),
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: 250.0,
                          width: MediaQuery.of(context).size.width,
                          padding:EdgeInsets.all(0.0),
                          child: SimpleViewPlayer(
                            "https://video.arkademi.com/Bootstrap/1/master.m3u8",
                            isFullScreen: false,
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            // Box decoration takes a gradient
                            gradient: LinearGradient(
                              // Where the linear gradient begins and ends
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                              // Add one stop for each color. Stops should increase from 0 to 1
                              stops: [0.5, 1.0],
                              colors: [
                                // Colors are easy thanks to Flutter's Colors class.
                                Color.fromRGBO(220, 220, 220, 1),
                                Colors.white,
                              ],
                            ),
                          ),
                          padding: EdgeInsets.only(
                            left: 15,
                            right: 15,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(
                                  top: 10,
                                ),
                                child: Text(
                                    "${widget.selectDATA[0]['data']['course']['name']}"),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                  top: 6,
                                ),
                                child: Text(
                                  "${widget.selectDATA[0]['data']['curriculum'][widget.ids]['title']}",
                                  style: TextStyle(
                                    fontSize: 22,
                                  ),
                                ),
                              ),
                              Padding(
                                  padding: EdgeInsets.only(
                                    top: 3,
                                  ),
                                  child: Row(
                                    children: <Widget>[
                                      Icon(Icons.timelapse),
                                      Text(
                                          "${widget.selectDATA[0]['data']['curriculum'][widget.ids]['duration']}"),
                                    ],
                                  )),
                              Padding(
                                padding: EdgeInsets.only(
                                  top: 10,
                                  bottom: 15,
                                ),
                                child: LinearPercentIndicator(
                                  width: MediaQuery.of(context).size.width - 30,
                                  lineHeight: 10.0,
                                  percent: 0.1,
                                  progressColor: Color.fromRGBO(254, 86, 23, 1),
                                  backgroundColor:
                                      Color.fromRGBO(241, 241, 241, 1),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(),
                          width: MediaQuery.of(context).size.width,
                          color: Color.fromRGBO(193, 193, 193, 1),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    tabIndex = 0;
                                  });
                                },
                                child: Container(
                                  color: tabIndex == 0
                                      ? Color.fromRGBO(75, 75, 75, 1)
                                      : Colors.transparent,
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(
                                          top: 10,
                                        ),
                                        width:
                                            MediaQuery.of(context).size.width /
                                                3,
                                        height: 40,
                                        child: Text(
                                          "Deskripsi",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w300,
                                            color: tabIndex == 0
                                                ? Colors.white
                                                : Colors.black,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                3,
                                        height: 3,
                                        color: tabIndex == 0
                                            ? Colors.orange
                                            : Colors.grey,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    tabIndex = 1;
                                  });
                                },
                                child: Container(
                                  color: tabIndex == 1
                                      ? Color.fromRGBO(75, 75, 75, 1)
                                      : Colors.transparent,
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(
                                          top: 10,
                                        ),
                                        width:
                                            MediaQuery.of(context).size.width /
                                                3,
                                        height: 40,
                                        child: Text(
                                          "Kurikulum",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w300,
                                            color: tabIndex == 1
                                                ? Colors.white
                                                : Colors.black,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                3,
                                        height: 3,
                                        color: tabIndex == 1
                                            ? Colors.orange
                                            : Colors.grey,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    tabIndex = 2;
                                  });
                                },
                                child: Container(
                                  color: tabIndex == 2
                                      ? Color.fromRGBO(75, 75, 75, 1)
                                      : Colors.transparent,
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(
                                          top: 10,
                                        ),
                                        width:
                                            MediaQuery.of(context).size.width /
                                                3,
                                        height: 40,
                                        child: Text(
                                          "Ulasan",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w300,
                                            color: tabIndex == 2
                                                ? Colors.white
                                                : Colors.black,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                3,
                                        height: 3,
                                        color: tabIndex == 2
                                            ? Colors.orange
                                            : Colors.grey,
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            top: 10,
                          ),
                          padding: EdgeInsets.only(
                            left: 15,
                            right: 15,
                            bottom: 30,
                          ),
                          width: MediaQuery.of(context).size.width,
                          child: content(),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => changeSelected(1),
          tooltip: 'Increment',
          backgroundColor: Colors.transparent,
          elevation: 0,
          child: SvgPicture.asset(
            "assets/bullseye.svg",
            height: 50,
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar: BottomAppBar(
          color: _selectedIndex == 1 ? Colors.yellow : Colors.white,
          shape: FollowerNotchedShape(inverted: true),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              InkWell(
                onTap: () {
                  changeSelected(0);
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: _selectedIndex == 0 ? Colors.white : Colors.white,
                      borderRadius:
                          BorderRadius.only(bottomRight: Radius.circular(100))),
                  height: 60,
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                  width: MediaQuery.of(context).size.width / 4 - 20,
                  child: Column(
                    children: <Widget>[
                      SvgPicture.asset(
                        "assets/angle-left.svg",
                        height: 30,
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  changeSelected(0);
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: _selectedIndex == 0 ? Colors.white : Colors.white,
                      borderRadius:
                          BorderRadius.only(bottomRight: Radius.circular(100))),
                  height: 60,
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                  width: MediaQuery.of(context).size.width / 4 - 20,
                  child: Column(
                    children: <Widget>[
                      SvgPicture.asset(
                        "assets/book-open.svg",
                        height: 25,
                      ),
                      Text("Lobi"),
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  changeSelected(1);
                },
                child: Container(
                  height: 50,
                  width: 80,
                  // color: Colors.red,
                  padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                  child: Text(
                    "Seri Selesai",
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  changeSelected(2);
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: _selectedIndex == 2 ? Colors.yellow : Colors.white,
                      borderRadius:
                          BorderRadius.only(bottomLeft: Radius.circular(100))),
                  height: 60,
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                  width: MediaQuery.of(context).size.width / 4 - 20,
                  child: Column(
                    children: <Widget>[
                      SvgPicture.asset(
                        "assets/indent.svg",
                        height: 25,
                      ),
                      Text("Direktori"),
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => KuisPage()),
                  );
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: _selectedIndex == 0 ? Colors.white : Colors.white,
                      borderRadius:
                          BorderRadius.only(bottomRight: Radius.circular(100))),
                  height: 60,
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                  width: MediaQuery.of(context).size.width / 4 - 20,
                  child: Column(
                    children: <Widget>[
                      SvgPicture.asset(
                        "assets/angle-right.svg",
                        height: 30,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  int _selectedIndex = 0;

  void changeSelected(int index) {}

  void _MulaiKelasPage() {}

  void _bergabung() {}

  Widget joinWidget(int hargaAsli, int hargaDiskon) {
    return InkWell(
      onTap: isUserLogin ? _MulaiKelasPage : _bergabung,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25),
          color: isUserLogin ? Colors.green : Colors.amber[900],
        ),
        height: 50,
        width: MediaQuery.of(context).size.width,
        child: isUserLogin
            ? Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Container(
                      child: Text(
                        "MULAI KELAS",
                        style: TextStyle(fontSize: 20, color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              )
            : Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: Container(
                      child: Text(
                        "BERGABUNG",
                        style: TextStyle(fontSize: 20, color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(
                        right: 10,
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20)),
                        color: Colors.deepOrange[300],
                      ),
                      width: MediaQuery.of(context).size.width / 2,
                      height: 50,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Text(
                            "Rp ${numberFormat(hargaDiskon)}",
                            style: TextStyle(
                              fontSize: 22,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                            textAlign: TextAlign.right,
                          ),
                          Text(
                            "Rp ${numberFormat(hargaAsli)}",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 12,
                            ),
                            textAlign: TextAlign.right,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
      ),
    );
  }

  Widget content() {
    if (tabIndex == 0) {
      return DeskripsiPage(
        title: "${widget.selectDATA[0]['data']['course']['name']}",
        content: "${widget.selectDATA[0]['data']['description']}",
      );
    } else if (tabIndex == 1) {
      return KurikulumPage(
        curriculum: widget.selectDATA[0]['data']['curriculum'],
      );
    } else {
      return UlasanPage(
        ulasanList: widget.selectDATA[0]['data']['reviews'],
      );
    }
  }
}
