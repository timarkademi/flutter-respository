import 'package:flutter/material.dart';
import 'package:arkademi/data/info.dart';

class InfoWidget extends StatefulWidget {
  @override
  _InfoWidgetState createState() => _InfoWidgetState();
}

class _InfoWidgetState extends State<InfoWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: infoList.length,
        itemBuilder: (context, index){
          double left = index == 0 ? 17 : 10;
          double right = index == infoList.length - 1 ? 17 : 0;
          return BuildItem(
            marginLeft: left,
            marginRight: right,
            info: infoList[index],
          );
        },
      ),
    );
  }


}

class BuildItem extends StatelessWidget {
  final double marginLeft;
  final double marginRight;
  final Info info;

  BuildItem({
    this.marginLeft,
    this.marginRight,
    this.info
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Card(
        elevation: 5,
        margin: EdgeInsets.only(
          left: marginLeft,
          right: marginRight,
          bottom: 10,
          top: 10,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10)
        ),
        child: Container(
          width: 170,
          height: 50,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/info-card.png"),
              fit: BoxFit.fill
            ),
          ),
          child: Row(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(
                  left: 10,
                  right: 25,
                ),
                child:Image.network(info.icon,
                  width: 40,
                  height: 40,),
              ),
              Expanded(
                child: Text(info.label,
                style: TextStyle(
                  color: Colors.white,
                  fontStyle: FontStyle.italic
                ),
                maxLines: 2,
                ),
              )
              

            ],
          ),
        ),
      ),
    );
  }
}