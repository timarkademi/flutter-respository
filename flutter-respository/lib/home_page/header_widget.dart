import 'package:arkademi/data/product.dart';
import 'package:arkademi/home_page/kelas_saya_widget.dart';
import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class HeaderWidget extends StatefulWidget {
  @override
  _HeaderWidgetState createState() => _HeaderWidgetState();
}

class _HeaderWidgetState extends State<HeaderWidget> {
  
  bool isUserLogin = false;
  String isUserAvatar = '';
  String isUserName;

List dataJSON;
int JsonView;

  Future<List>  ambildata() async {
  http.Response hasil = await http.get(
    Uri.encodeFull("https://staging.arkademi.com/api.php"), headers: {
      "Accept":"application/json"
    }
  );
  dataJSON = json.decode(hasil.body);
  if(dataJSON == null){
    JsonView = 0;
  }else{
    JsonView = dataJSON.length;
  }
  this.setState((){
    return dataJSON.toList();
});
}


  @override
  void initState() {
    super.initState();
    init();
    ambildata();
  }

  void init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    isUserLogin = prefs.getBool('user_login') ?? false;
    isUserAvatar = prefs.getString('user_avatar') ?? '';
    isUserName = prefs.getString('user_name') ?? '';
    setState(() {
      
    });
  }

  @override
  Widget build(BuildContext context) {
    //print("${} iki ");
    return Stack(
      children: <Widget>[
        Image.asset('assets/header.png'),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(20, 30, 20, 10),
              child: Image.asset('assets/logo-arkademi-white.png',
                height: 25,
              ),
            ),
            isUserLogin ? userCard() : Container(
              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: Row(
                children: <Widget>[
                  Icon(Mdi.accountCircle, size: 45, color: Colors.white,),
                  Container(width: 7,),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Text("Halo ", style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                          ),),
                          Text("tamu", style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),),
                          
                        ],
                      ),
                      Text(
                        "Mau belajar apa hari ini?", 
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                        ),
                        maxLines: 2,
                      )
                    ],
                  )
                ],
              ),
            ),
            
            isUserLogin ? KelasSayaWidget(
              title: "KELAS ANDA",
              subtitle: "Lanjutkan belajar di kelas yang anda ikuti",
              list: productList,
              viewAllButton: () {
              },
            ) : Container(),
            
            // BANNER
            Container(
              margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
              height: (100/318* 0.8 * MediaQuery.of(context).size.width) + 6,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: (dataJSON == null) ? 0 : dataJSON.length,
                itemBuilder: (context, index){
                  var width = 0.8 * MediaQuery.of(context).size.width;
                  return InkWell(
                    child: Container(
                      margin: EdgeInsets.only(
                        left: index == 0 ? 20 : 5,
                        right: index == 1 ? 20 : 0,
                      ),
                      width: width,
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)
                        ),
                        elevation: 4,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Image.network(dataJSON[index]['image_link']),
                        ),
                      )
                    ),
                  );
                },
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: 5,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  for(int i = 0; i <  3 ; i++) 
                    Icon(Mdi.circleOutline, size: 10, color: Colors.blue,),
                  
                ],
              ),
            ),
          ],
        ),
        



        
        
      ],
    );
  }


  Widget userCard() {
    return Container(
      margin: EdgeInsets.only(
        left: 15,
        right: 15,
      ),
      padding: EdgeInsets.only(
        left: 30,
        right: 30,
        top: 25,
      ),
      height: MediaQuery.of(context).size.width * 0.46,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/home-user-card.png"),
          fit: BoxFit.contain
        )
      ),
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Image.network(
                isUserAvatar,
                width: 50,
                height: 50,
              ),
              Container(width: 5,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    isUserName,
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                  Text(
                    "Mau belajar apa hari ini?",
                    style: TextStyle(
                      fontSize: 18,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                  Container(height: 5,),
                  Row(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Card(
                            elevation: 5,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            color: Colors.grey[350],
                            child: Container(
                              padding: EdgeInsets.only(
                                left: 12,
                                right: 12,
                                top: 3,
                                bottom: 3,
                              ),
                              child: Text(
                                "KELAS DIIKUTI",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold
                                ),
                              ),
                            ),
                          ),
                          Text(
                            "0",
                            style: TextStyle(
                              fontSize: 26,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      
                      Container(width: 5,),
                      Column(
                        children: <Widget>[
                          Card(
                            elevation: 5,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            color: Colors.grey[350],
                            child: Container(
                              padding: EdgeInsets.only(
                                left: 12,
                                right: 12,
                                top: 3,
                                bottom: 3,
                              ),
                              child: Text(
                                "KUIS SELESAI",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold
                                ),
                              ),
                            ),
                          ),
                          Text(
                            "0",
                            style: TextStyle(
                              fontSize: 26,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}