import 'package:arkademi/kelas_page/kelas_page.dart';
import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class ProductWidget extends StatefulWidget {
  final int id;
  final String title;
  final String subtitle;
  final VoidCallback viewAllButton;
  final  isUserLogin;
  final isJoinKelas;
  String tokenValue;
  
  final bool showStar;

  ProductWidget({
    this.id,
    this.title,
    this.subtitle,
    this.viewAllButton,
     this.showStar,
     this.isJoinKelas,
     this.isUserLogin,
     this.tokenValue,
  });
  @override
  _ProductWidgetState createState() => _ProductWidgetState(
    id : id,
    title: title,
    subtitle: subtitle,
    viewAllButton: viewAllButton,
    showStar: showStar,
    isUserLogin: isUserLogin,
    isJoinKelas: isJoinKelas,
    tokenValue:  tokenValue
  );
}

class _ProductWidgetState extends State<ProductWidget> {
  int id;
  String title;
  String subtitle;
  bool  isUserLogin;
  bool isJoinKelas;
  VoidCallback viewAllButton;
  List list;
  bool showStar = false;
  String tokenValue; 

  _ProductWidgetState({
    this.id,
    this.title,
    this.subtitle,
    this.viewAllButton,
    this.list,
    this.showStar,
    this.isJoinKelas,
    this.isUserLogin,
    this.tokenValue
  });



List dataJSON = [];

  Future<List>  ambildata() async {
  http.Response hasil = await http.get(
    Uri.encodeFull("https://staging.arkademi.com/wp-json/wplms/v1/course"), headers: {
      "Accept":"application/json"
    }
  );
  dataJSON = jsonDecode(hasil.body);
  this.setState((){
    dataJSON.toList();
    
});
}



  @override
  void initState() {
      ambildata();
      tokenValue;
  }

  @override
  Widget build(BuildContext context) {

    return InkWell(
   //   onTap: () {
     //   Navigator.push(
      //    context,MaterialPageRoute(builder: (context) => KelasPage()),
      //  );
     // },

      child: Container(
        margin: EdgeInsets.only(
          top: 10,
          bottom: 0,
        ),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(title, style: TextStyle(
                                fontSize: 18,
                                color: Colors.grey[900]
                              ),),
                              showStar == true ? Row(
                                children: <Widget>[
                                  Container(width: 5,),
                                  Icon(Mdi.star, color: Colors.orange, size: 10,),
                                  Icon(Mdi.star, color: Colors.orange, size: 10,),
                                  Icon(Mdi.star, color: Colors.orange, size: 10,),
                                  Icon(Mdi.star, color: Colors.orange, size: 10,),
                                  Icon(Mdi.star, color: Colors.orange, size: 10,),
                                ],
                              ) : Container(),
                            ],
                          ),
                        ],
                      ),
                      Text(subtitle, style: TextStyle(
                        fontSize: 12,
                        color: Colors.grey[700],
                      ),),
                    ],
                  ),
                  InkWell(
                    onTap: () {

                    },
                    child: Icon(Mdi.menu, color: Colors.grey[400],size: 30,),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: 12,
              ),
              height: 200,

              child: ListView.builder(
                itemCount:  dataJSON.length ,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {

                return new GestureDetector(
              onTap: ()=>Navigator.of(context).push(
                      new MaterialPageRoute(
                        builder: (BuildContext context)=> new KelasPage(isUserLogin: isUserLogin, isJoinKelas: isJoinKelas, index : index,dataJSON:dataJSON, tokenValue:tokenValue)
                      )
                    ),
                  child: buildItem(
                    index: index,
                    imageUrl: dataJSON[index]['data']['featured_image'] == null ? 0 : dataJSON[index]['data']['featured_image'],
                    title: dataJSON[index]['data']['name'] == null ? 0 : dataJSON[index]['data']['name'],
                    rating: dataJSON[index]['data']['average_rating'] == null ? 0 : dataJSON[index]['data']['average_rating'],
                    student: dataJSON[index]['data']['total_students'] == null ? 0 : dataJSON[index]['data']['total_students'],
                    price: dataJSON[index]['data']['price'] is String ? "GRATIS" : "",
                    priceAfterDisc: dataJSON[index]['data']['price_html'] is String ? "GRATIS" : dataJSON[index]['data']['price'],
                  ),
                );

                },
              ),

            
            )
          ],
        ),
      ),
    );
  }
}

class buildItem extends StatefulWidget {


  
  final int index;
  final String imageUrl;
  final String title;
  final String rating;
  final int student;
  final String price;
  final String priceAfterDisc;

  buildItem({
    this.index,
    this.imageUrl,
    this.title,
    this.rating,
    this.student,
    this.price,
    this.priceAfterDisc,
  });
  @override
  _buildItemState createState() => _buildItemState(
    index: index,
    title: title,
    rating: rating,
    student: student,
    imageUrl: imageUrl,
    price: price,
    priceAfterDisc: priceAfterDisc,
  );
}

class _buildItemState extends State<buildItem> {


  
  int index;
  String imageUrl;
  String title;
  String rating;
  int student;
  String price;
  String priceAfterDisc;

  _buildItemState({
    this.index,
    this.imageUrl,
    this.title,
    this.rating,
    this.student,
    this.price,
    this.priceAfterDisc,
  });


  @override
  Widget build(BuildContext context) {
    return Container(
      width: 175,
      margin: EdgeInsets.only(
        left: index == 0 ? 16 : 5,
        right: index == 2 ? 16 : 0,
      ),
      child: Stack(
        children: <Widget>[
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 180,
            child: Card(
              color: Color.fromRGBO(241, 241, 241, 1),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8)
              ),
              child: Column(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(5),
                    ),
                    child: Image.network(this.imageUrl),
                  ),
                  Container(
                    height: 84,
                    padding: EdgeInsets.only(
                      left: 5,
                      right: 5,
                      top: 5,
                    ),
                    child: Column(
                      children: <Widget>[
                        Text(this.title, 
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: Colors.black
                        ),),
                        Container(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Icon(Mdi.star, color: Colors.orange, size: 10,),
                                Icon(Mdi.star, color: Colors.orange, size: 10,),
                                Icon(Mdi.star, color: Colors.orange, size: 10,),
                                Icon(Mdi.star, color: Colors.orange, size: 10,),
                                Icon(Mdi.star, color: Colors.orange, size: 10,),
                                Text("(${this.rating})", style: TextStyle(
                                  fontSize: 10,
                                ),),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Icon(Mdi.accountGroup, size: 10, color: Colors.black,),
                                Text("${this.student} siswa", style: TextStyle(
                                  fontSize: 10
                                ),)
                              ],
                            )
                          ],
                        ),

                      ],
                    ),
                  ),

                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              top: 160,
            ),
            padding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 25,
              child: FlatButton(
                color: Color.fromRGBO(9, 120, 191, 1),
                onPressed: () {

                },
                child: _priceWidget(),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _priceWidget() {

    if (price == 0 && priceAfterDisc == null) {
      return Text("GRATIS", style: TextStyle(
                fontSize: 16,
                color: Colors.white,
              ),);
    } else if(price == 0 && priceAfterDisc == null) {
      return Text(price.toString(), style: TextStyle(
                  fontSize: 16,
                  color: Colors.white,
                ),);
    } else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(priceAfterDisc.toString(), style: TextStyle(
                  fontSize: 16,
                  color: Colors.white,
                ),),
                Text(price.toString(), style: TextStyle(
                  fontSize: 11,
                  decoration: TextDecoration.lineThrough,
                  color: Colors.white,
                ),),
              ],
            );
    }
  }
}