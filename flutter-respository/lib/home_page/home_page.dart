import 'package:arkademi/home_page/info_widget.dart';
import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';
import 'blog_widget.dart';
import 'category_widget.dart';
import 'header_widget.dart';
import 'product_widget.dart';
//import 'package:arkademi/data/product.dart';

class HomePage extends StatefulWidget {


 final bool isJoinKelas;
  final bool isUserLogin;
  String tokenValue;

  HomePage({

    this.isJoinKelas,
    this.isUserLogin,
    this.tokenValue

    

  });


  @override
  _HomePageState createState() => _HomePageState(
    isJoinKelas: isJoinKelas,
    isUserLogin : isUserLogin,
    tokenValue: tokenValue,
  );
}

class _HomePageState extends State<HomePage> {

  bool isJoinKelas;
  bool isUserLogin;
  String tokenValue;

  _HomePageState({
    this.isJoinKelas,
    this.isUserLogin,
    this.tokenValue
  });

  

  @override
  void initState() {
    super.initState();
    isUserLogin;
    isJoinKelas;
    tokenValue;
  }


  
  @override
  Widget build(BuildContext context) {
    
    return isUserLogin == false ? ScaffoldNoLogin(isJoinKelas,isUserLogin)  : ScaffoldLogin(isJoinKelas,isUserLogin,tokenValue);
  }
}

Widget ScaffoldNoLogin(isJoinKelas,isUserLogin){
 
return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              HeaderWidget(),
              ProductWidget(
                title: "Kelas & Tool Gratis",
                subtitle: "Perjalanan anda dimulai dari sini",  
                isUserLogin : false,
                isJoinKelas : false,
                viewAllButton: () {
                },
              ),
              InfoWidget(),
              ProductWidget(
                title: "Kelas populer",
                subtitle: "Kelas paling diminati dan jumlah siswa terbanyak",
                isUserLogin : false,
                isJoinKelas : false,
                showStar: true,
                viewAllButton: () {                 
                },
              ),
              CategoryWidget(),
              BlogWidget(),
              Container(
                margin: EdgeInsets.only(
                  top: 15,
                ),
                padding: EdgeInsets.only(
                  left: 20,
                  right: 20,
                ),
                child: Column(
                  children: <Widget>[
                    Divider(height: 3,),
                    Container(height: 20,),
                    Text("ARKADEMI V2.1", style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),),
                    Container(height: 5,),
                    Text("All rights reserved PT Arkademi Daya Indonesia 2017-2020", style: TextStyle(
                      fontSize: 10,
                    ),),
                    FlatButton.icon(
                      onPressed: () {
                      },
                      icon: Icon(Mdi.email, color: Colors.grey[600], size: 16,),
                      label: Text("KONTAK", style: TextStyle(
                        color: Colors.grey[600],
                        fontSize: 14,
                      ),),
                    ),
                    Container(height: 50,),
                  ],
                )
              ),
              
            ],
          ),
        ),
      ),
    );




}


Widget ScaffoldLogin(isJoinKelas,isUserLogin, tokenValue){


return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              HeaderWidget(),
              ProductWidget(
                title: "Kelas & Tool Gratis",
                subtitle: "Perjalanan anda dimulai dari sini",  
                isUserLogin : true,
                isJoinKelas : true,
                tokenValue : tokenValue,
                viewAllButton: () {
                },
              ),
              InfoWidget(),
              ProductWidget(
                title: "Kelas populer",
                subtitle: "Kelas paling diminati dan jumlah siswa terbanyak",
                isUserLogin : true,
                isJoinKelas : true,
                showStar: true,
                tokenValue : tokenValue,
                viewAllButton: () {                 
                },
              ),
              CategoryWidget(),
              BlogWidget(),
              Container(
                margin: EdgeInsets.only(
                  top: 15,
                ),
                padding: EdgeInsets.only(
                  left: 20,
                  right: 20,
                ),
                child: Column(
                  children: <Widget>[
                    Divider(height: 3,),
                    Container(height: 20,),
                    Text("ARKADEMI V2.1", style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),),
                    Container(height: 5,),
                    Text("All rights reserved PT Arkademi Daya Indonesia 2017-2020", style: TextStyle(
                      fontSize: 10,
                    ),),
                    FlatButton.icon(
                      onPressed: () {
                      },
                      icon: Icon(Mdi.email, color: Colors.grey[600], size: 16,),
                      label: Text("KONTAK", style: TextStyle(
                        color: Colors.grey[600],
                        fontSize: 14,
                      ),),
                    ),
                    Container(height: 50,),
                  ],
                )
              ),
              
            ],
          ),
        ),
      ),
    );


}