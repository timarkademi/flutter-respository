import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';
class CategoryWidget extends StatefulWidget {
  @override
  _CategoryWidgetState createState() => _CategoryWidgetState();
}

class _CategoryWidgetState extends State<CategoryWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: 20,
        bottom: 0,
      ),
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Kategori", style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey[900]
                    ),),
                    Text("Berbagai kelas untuk meningkatkan skill anda", style: TextStyle(
                      fontSize: 12,
                      color: Colors.grey[700],
                    ),),
                  ],
                ),
                
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left: 15,
              right: 15,
              top: 10,
            ),
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 90,
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      stops: [0.1, 1.0],
                      colors: [
                        Colors.white,
                        Colors.grey[300]
                      ]
                    )
                  ),
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      categoryItem("assets/icon-company.png","Korporat",(){
                        _showModalSheet();
                      }),
                      categoryItem("assets/icon-outlet.png","Kewirausahaan", (){
                        _showModalSheet();
                      }),
                      categoryItem("assets/icon-marketing.png", "Pemasaran",(){
                        _showModalSheet();
                      }),
                      categoryItem("assets/icon-more.png", "Semua",(){
                        _showModalSheet();
                      }),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ]
      ),
    );
  }

  Widget categoryItem(String fileName, String title, VoidCallback onTap) {
    return InkWell(
      onTap: onTap,
      child: Container(
        child: Column(
          children: <Widget>[
            Image.asset(fileName, width: 50, height: 50,fit: BoxFit.cover,),
            Text(title, style: TextStyle(
              fontSize: 11,
            ),)
          ],
        ),
      ),
    );
  }

  void _showModalSheet() {
    List<Widget> bottomModalList = [
      categoryItem(
        "assets/icon-marketing.png", 
        "Pemasaran", 
        _showModalSheet,),
      categoryItem(
        "assets/icon-marketing.png", 
        "Pemasaran", 
        _showModalSheet,),
      categoryItem(
        "assets/icon-marketing.png", 
        "Pemasaran", 
        _showModalSheet,),
      categoryItem(
        "assets/icon-marketing.png", 
        "Pemasaran", 
        _showModalSheet,),
      categoryItem(
        "assets/icon-marketing.png", 
        "Pemasaran", 
        _showModalSheet,),
      categoryItem(
        "assets/icon-marketing.png", 
        "Pemasaran", 
        _showModalSheet,),
    ];
    showModalBottomSheet(context: context, builder: (builder) {
      return Container(
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
          color: Colors.white,
        ),
        child: Column(
          children: <Widget>[
            Container(
              width: 150,
              height: 5,
              decoration: BoxDecoration(
                color: Colors.grey[400],
                borderRadius: BorderRadius.circular(20)
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: 20,
                bottom: 20,
              ),
              child: Text("Kategori",
              style: TextStyle(
                fontSize: 14,
              ),),
            ),
            GridView.builder(
              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 100.0,
              ), 

              shrinkWrap: true,
              itemCount: bottomModalList.length,
              itemBuilder: (context,index) {
                return bottomModalList[index];
              },
            ),
          ],
        )
      );
    }, backgroundColor: Colors.transparent);
  }
}