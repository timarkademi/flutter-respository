import 'package:arkademi/kelas_page/kelas_page.dart';
import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';
import 'package:arkademi/data/product.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
class KelasSayaWidget extends StatefulWidget {
  final String title;
  final String subtitle;
  final VoidCallback viewAllButton;
  final List<Product> list;
  final bool showStar;

  KelasSayaWidget({
    this.title,
    this.subtitle,
    this.viewAllButton,
    this.list,
    this.showStar,
  });
  @override
  _KelasSayaWidgetState createState() => _KelasSayaWidgetState(
    title: title,
    subtitle: subtitle,
    viewAllButton: viewAllButton,
    list: list,
    showStar: showStar,
  );
}

class _KelasSayaWidgetState extends State<KelasSayaWidget> {
  String title;
  String subtitle;
  VoidCallback viewAllButton;
  List<Product> list;
  bool showStar = false;

  _KelasSayaWidgetState({
    this.title,
    this.subtitle,
    this.viewAllButton,
    this.list,
    this.showStar,
  });

 

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: 10,
        bottom: 0,
      ),
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text(title, style: TextStyle(
                              fontSize: 18,
                              color: Colors.grey[900]
                            ),),
                            showStar == true ? Row(
                              children: <Widget>[
                                Container(width: 5,),
                                Icon(Mdi.star, color: Colors.orange, size: 10,),
                                Icon(Mdi.star, color: Colors.orange, size: 10,),
                                Icon(Mdi.star, color: Colors.orange, size: 10,),
                                Icon(Mdi.star, color: Colors.orange, size: 10,),
                                Icon(Mdi.star, color: Colors.orange, size: 10,),
                              ],
                            ) : Container(),
                          ],
                        ),
                      ],
                    ),
                    Text(subtitle, style: TextStyle(
                      fontSize: 12,
                      color: Colors.grey[700],
                    ),),
                  ],
                ),
                
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              top: 12,
            ),
            height: 160,
            child: ListView.builder(
              itemCount: list.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return buildItem(
                  index: index,
                  imageUrl: list[index].imageUrl,
                  title: list[index].title,
                  rating: list[index].rating,
                  student: list[index].student,
                  price: list[index].price,
                  priceAfterDisc: list[index].priceAfterDisc,
                );
              },
            ),
          )
        ],
      ),
    );
  }
}

class buildItem extends StatefulWidget {
  final int index;
  final String imageUrl;
  final String title;
  final double rating;
  final int student;
  final int price;
  final int priceAfterDisc;

  buildItem({
    this.index,
    this.imageUrl,
    this.title,
    this.rating,
    this.student,
    this.price,
    this.priceAfterDisc,
  });
  @override
  _buildItemState createState() => _buildItemState(
    index: index,
    title: title,
    rating: rating,
    student: student,
    imageUrl: imageUrl,
    price: price,
    priceAfterDisc: priceAfterDisc,
  );
}

class _buildItemState extends State<buildItem> {
  int index;
  String imageUrl;
  String title;
  double rating;
  int student;
  int price;
  int priceAfterDisc;

  _buildItemState({
    this.index,
    this.imageUrl,
    this.title,
    this.rating,
    this.student,
    this.price,
    this.priceAfterDisc,
  });


  
  @override
  Widget build(BuildContext context) {
    return InkWell(
      // onTap: () {
      //  Navigator.push(
      //    context,
      //    MaterialPageRoute(builder: (context) => KelasPage()),
      //  );
      //},
      child: Container(
        width: 120,
        margin: EdgeInsets.only(
          left: index == 0 ? 16 : 5,
          right: index == 2 ? 16 : 0,
        ),
        child: Stack(
          children: <Widget>[
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 150,
              child: Card(
                color: Color.fromRGBO(241, 241, 241, 1),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)
                ),
                child: Column(
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(5),
                        topRight: Radius.circular(5),
                      ),
                      child: Image.network(imageUrl),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        left: 5,
                        right: 5,
                        top: 5,
                      ),
                      child: Column(
                        children: <Widget>[
                          Text(title, 
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 12,
                          ),),
                          Container(height: 10,),
                          

                        ],
                      ),
                    ),
                    LinearPercentIndicator(
                      width: 110,
                      lineHeight: 8.0,
                      percent: 0.9,
                      progressColor: Colors.blue,
                    ),
                    
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: 130,
              ),
              padding: EdgeInsets.only(
                left: 15,
                right: 15,
              ),
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Card(
                  color: Color.fromRGBO(30, 130, 15, 1),
                  child: Container(
                    padding: EdgeInsets.only(
                      top: 3,
                      bottom: 3,
                    ),
                    child: Text(
                      "LANJUTKAN", 
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 10,
                        color: Colors.white,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                )
              ),
            ),
          ],
        ),
      ),
    );
  }

}