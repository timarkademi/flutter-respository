import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';
import 'package:arkademi/kelas_page/kelas_page.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class BlogWidget extends StatefulWidget {
  @override
  _BlogWidgetState createState() => _BlogWidgetState();
}

class _BlogWidgetState extends State<BlogWidget> {

List dataJSON = [];

  Future<List>  ambildata() async {

  http.Response hasil = await http.get(
    Uri.encodeFull("https://staging.arkademi.com/wp-json/wplms/v1/blog"), headers: {
      "Accept":"application/json"
    }
  );

  this.setState((){
    dataJSON = json.decode(hasil.body);
});
}

@override
  void initState() {
    super.initState();
    ambildata();
  }
  
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: 20,
        bottom: 0,
      ),
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Blog", style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey[900]
                    ),),
                    Text("Bacaan bergizi kewirausahaan khas Arkademi", style: TextStyle(
                      fontSize: 12,
                      color: Colors.grey[700],
                    ),),
                  ],
                ),
                InkWell(
                  onTap: () {

                      Navigator.push(   context,
                        MaterialPageRoute(builder: (context) => KelasPage()), 
                      );


                  },
                  child: Icon(Mdi.menu),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(0, 12, 0, 0),
            padding: EdgeInsets.fromLTRB(18, 0, 18, 0),
            child: Column(
              children: <Widget>[
                for(int i = 0; i < dataJSON.length; i++) blogItem(i)
              ],
            ),
          ),
          
        ]
      ),
    );
  }

  Widget blogItem(i) {
    return Container(
      margin: EdgeInsets.only(
        bottom: 8,
      ),



      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: 90,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8)
          ),
          color: Colors.grey[200],
          child: InkWell(

            onTap: () {},


            child: Row(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(50),
                    topLeft: Radius.circular(8),
                    bottomLeft: Radius.circular(8),
                  ),
                  child: Image.network(dataJSON[i]['featured_image'],
                    height: 90,
                    width: 110,
                    fit: BoxFit.cover,
                  ),
                ),
                
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(
                      left: 15,
                      right: 15
                    ),
                    child: Text(dataJSON[i]['title'],style: TextStyle(
                      fontSize: 14,

                    ),
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
              ],
            )
          ),
        ),
      ),






    );
  }
}